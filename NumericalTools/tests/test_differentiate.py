"""
Module to test various Differentiation operations

Usage:
    test_differentiate.py [options]

Options:
    --domain=<d>   Comma separated list giving xlo,xhi [default: -2,4]
    --grid=<g>     Specify grid, uniform, random, cheb [default: cheb]
    --extrema      Use extrema grid [default: False]
    --endpoints     Use zeros grid and include endpoints [default: False]
    --matrix       Use chebyshev DM method [default: False]
    --high-res     Include high resolutions in convergence test [default: False]
    --save         Save plot, do not display it [default: False]
    --verbose      Print more information to screen [default: False]
"""
from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from testing_utilities import get_error, get_function
from NumericalTools import differentiate
from NumericalTools.misc import grids

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    high_res = args.pop('--high-res', False)
    grid = args.pop('--grid', "cheb")
    extrema = args.pop('--extrema', False)
    endpoints = args.pop('--endpoints', False)
    cheb_matrix = args.pop('--matrix', False)
    saveplot = args.pop('--save', False)
    verbose = args.pop('--verbose', False)
    domain = args.pop('--domain', '-2,4')

    if (high_res):
        Ns = [9, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192]
    else:
        Ns = [9, 16, 32, 64, 128, 256, 512, 1024]

    domain = [float(domain.split(",")[0]), float(domain.split(",")[1])]
    xlo, xhi = domain

    if (grid == 'cheb'):
        methods = ['cheb', 'fd', 'fd']; orders = ['', '4', '6']
    elif (grid == 'uniform'):
        methods = ['fd', 'fd', 'fourier']; orders = ['4', '6', '']
    else:
        methods = ['fd', 'fd']; orders = ['4', '6']
    funcs = ['sin', 'gauss', 'exp']

    zero_tol = 1e-16
    max_tol = 1e50

    errors = {} # hold all errors
    for func in funcs:
        if (verbose):
            print("starting func = {}".format(func))
        f, dfdx, _I, funcname = get_function(func)

        err = {}
        for i in range(len(methods)):
            order = orders[i]; method = methods[i]

            d_error = []
            for n in Ns:
                if (verbose):
                    print("\tworking on N = {}".format(n))
                if (grid == 'cheb'):
                    G = grids.ChebGrid(n, a=xlo, b=xhi, zeros=not(extrema),
                                       endpoints=endpoints)
                    x = G.xab
                elif (grid == 'uniform'):
                    G = grids.UniformGrid(n, a=xlo, b=xhi)
                    x = G.x
                elif (grid == 'random'):
                    G = grids.RandomGrid(n, a=xlo, b=xhi, seed=43)
                    x = G.x
                else:
                    print("\n---ERROR: unrecognized grid = {}\n".format(grid))
                    return

                fx = f(x); fderiv = dfdx(x)

                deriv = differentiate.derivative(fx, x, data_reversed=False,
                                   method=method, order=order, matrix=cheb_matrix,
                                   zeros=not(extrema), endpoints=endpoints)

                # get L2 error
                dx = np.mean(x[1:] - x[:-1])
                L2d = np.sqrt(dx*np.sum((deriv - fderiv)**2))
                if (L2d > max_tol):
                    L2d = max_tol
                elif (L2d < zero_tol):
                    L2d = zero_tol
                d_error.append(L2d)
                if (verbose):
                    print("\t\tdifferentiation L2 error = {}".format(L2d))

            # convert to arrays
            d_error = np.array(d_error)

            err[method+order] = [1.*np.array(Ns), d_error]

        errors[func] = err

    # report
    print("\nInputs:")
    print("\tdomain = {}".format(domain))
    print("\tgrid = {}".format(grid))
    print("\textrema = {}".format(extrema))

    # plots
    plt.clf()
    N = errors[funcs[0]][methods[0]+orders[0]][0]; ind = 0
    e = errors[funcs[0]][methods[0]+orders[0]][1]
    plt.plot(N, e[ind]*(N/N[ind])**-4,label=r'$O(N^{-4})$', linestyle='--',color='k')
    plt.plot(N, e[ind]*(N/N[ind])**-6,label=r'$O(N^{-6})$', linestyle='--',color='k')
    for key in errors.keys():
        for m in errors[key].keys():
            N    = errors[key][m][0]
            derr = errors[key][m][1]
            plt.plot(N, derr, label=m+', '+key, marker='x', linestyle=':')

    plt.title("Derivative Convergence")
    plt.legend(loc='upper right', ncol=1)
    plt.xlabel('N'); plt.ylabel('error')
    plt.xscale('log'); plt.yscale('log')
    plt.xlim(0.5*Ns[0], 15*Ns[-1])

    if (saveplot):
        output = "test_differentiate.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()
    print()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

