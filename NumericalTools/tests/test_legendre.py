"""
Module to test various Legendre operations

Usage:
    test_legendre.py [options]

Options:
    --domain=<d>   Comma separated list giving xlo,xhi [default: -2,4]
    --endpoints    Use zeros grid and include endpoints [default: False]
    --high-res     Include high resolutions in convergence test [default: False]
    --save         Save plot, do not display it [default: False]
    --verbose      Print more information to screen [default: False]
"""
from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from testing_utilities import get_error, get_function
from NumericalTools import legendre
from NumericalTools.misc import grids

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    high_res = args.pop('--high-res', False)
    saveplot = args.pop('--save', False)
    verbose = args.pop('--verbose', False)
    domain = args.pop('--domain', '-2,4')

    if (high_res):
        Ns = [4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192]
    else:
        Ns = [4, 8, 16, 32, 64, 128, 256, 512, 1024]

    domain = [float(domain.split(",")[0]), float(domain.split(",")[1])]
    xlo, xhi = domain

    funcs = ['sin', 'exp', 'gauss']

    # setting this to True uses a "non-standard" linear transformation
    # when using the legendre zeros grid. this transformation
    # maps x in the open interval (-1,1) to r in the closed interval [a,b]
    # so the grid is a legendre zeros grid that includes the boundary
    # values explicitly, a normal zeros grids does not
    endpoints = args.pop('--endpoints', False)

    zero_tol = 1e-16
    max_tol = 1e50

    errors = {} # hold all errors
    for func in funcs:
        if (verbose):
            print("starting func = {}".format(func))
        f, dfdx, _I, funcname = get_function(func)
        # when calculating I(x), make sure it satisfies I(xlo) = 0, in keeping with
        # the convention of the Legendre class
        def I(x):
            return _I(x) - _I(xlo)

        exact_def_integ = I(xhi) - I(xlo)
        if (abs(exact_def_integ) < zero_tol):
            exact_def_integ = zero_tol

        d_error = []; i_error = [] # these will be L2 errors
        def_integ_abs_err = []     # absolute error in definite integration
        def_integ_rel_err = []     # relative error in definite integration
        for n in Ns:
            if (verbose):
                print("\tworking on N = {}".format(n))

            G = grids.LegendreGrid(n, a=xlo, b=xhi, endpoints=endpoints)
            x = G.xab

            # build legendre object
            leg = legendre.Legendre(f(x), a=xlo, b=xhi, endpoints=endpoints)

            # definite integration
            def_int = leg.integ()
            abs_err, rel_err = get_error(def_int, exact_def_integ, zero_tol=zero_tol)
            def_integ_abs_err.append(abs(abs_err))    # store errors
            def_integ_rel_err.append(abs(rel_err))
            if (verbose):
                print("\t\tdefinite integral abs error = {}".format(abs(abs_err)))
                print("\t\tdefinite integral rel error = {}".format(abs(rel_err)))

            # indefinite integration
            _indef = leg.antideriv(x)    # calc indef integral
            exact_indef = I(x)           # get analytic indef integral
            dx = np.mean(x[1:] - x[:-1]) # get L2 error
            L2i = np.sqrt(dx*np.sum((_indef - exact_indef)**2))
            if (L2i > max_tol):
                L2i = max_tol
            elif (L2i < zero_tol):
                L2i = zero_tol
            i_error.append(L2i)
            if (verbose):
                print("\t\tindefinite integral L2 error = {}".format(L2i))

            _dfdx = leg.deriv(x)         # calc deriv and evaluate at x
            exact_dfdx = dfdx(x)         # get analytic deriv
            dx = np.mean(x[1:] - x[:-1]) # get L2 error
            L2d = np.sqrt(dx*np.sum((_dfdx - exact_dfdx)**2))
            if (L2d > max_tol):
                L2d = max_tol
            elif (L2d < zero_tol):
                L2d = zero_tol
            d_error.append(L2d)
            if (verbose):
                print("\t\tdifferentiation L2 error = {}".format(L2d))

        # convert to arrays
        d_error = np.array(d_error)
        i_error = np.array(i_error)
        def_integ_abs_err = np.array(def_integ_abs_err)
        def_integ_rel_err = np.array(def_integ_rel_err)

        errors[func] = [np.array(Ns), d_error, i_error, def_integ_abs_err, def_integ_rel_err]

    # report
    print("\nInputs:")
    print("\tdomain = {}".format(domain))

    # plots
    plt.clf()
    for key in errors.keys():
        N      = errors[key][0]
        derr   = errors[key][1]
        ierr   = errors[key][2]
        deferr = errors[key][4]

        plt.plot(N, deferr, label='def I, '+key, marker='x', linestyle=':')
        plt.plot(N, ierr,   label='indef, '+key, marker='x', linestyle=':')
        plt.plot(N, derr,   label='df/dx, '+key, marker='x', linestyle=':')

    if (endpoints):
        plt.title("Legendre Convergence, include endpoints")
    else:
        plt.title("Legendre Convergence")
    plt.legend(loc='best')
    plt.xlabel('N'); plt.ylabel('error')
    plt.xscale('log'); plt.yscale('log')

    if (saveplot):
        output = "test_legendre1.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()
    print()

    plt.clf()
    plt.plot(x, leg(x), linestyle='', marker='x', label='numeric f(x)')
    plt.plot(x, f(x),    linestyle='-', label='exact f(x)')
    plt.plot(x, leg.deriv(x), linestyle='', marker='x', label='numeric dfdx')
    plt.plot(x, exact_dfdx,    linestyle='-', label='exact dfdx')
    plt.plot(x, leg.antideriv(x), linestyle='', marker='x', label='numeric I(x)')
    plt.plot(x, exact_indef,       linestyle='-', label='exact I(x)')
    plt.legend(loc='best')
    plt.xlabel('x'); plt.ylabel('function value')
    plt.title("f(x), dfdx(x), I(x)")

    if (saveplot):
        output = "test_legendre2.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()
    print()


if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

