"""
Module to test various plotting operations

Usage:
    test_plotting.py [options]

Options:
    --N=<n>        Number of grid points [default: 128]
    --yx           Use the 'yx' ordering, default is 'xy' [default: False]
    --save         Save the image [default: False]
"""
from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from NumericalTools import plotting

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    N = int(args.pop('--N', '128'))
    save = args.pop('--save', False)
    yx = args.pop('--yx', False)

    print("\nTesting the plot2d module...")
    x = np.arange(-2*np.pi, 2*np.pi, 4*np.pi/N)
    y = np.arange(-np.pi, np.pi, 2*np.pi/N)

    plt.clf()
    if (not yx):
        data = np.zeros((len(x), len(y)))
        for i in range(len(x)):
            xx = x[i]
            for j in range(len(y)):
                yy = y[j]
                # data = data(x,y)
                data[i,j] = np.sin(2*yy)*np.exp(-yy**2/(0.75*np.pi)**2)*np.cos(xx)

        im = plotting.plot2d(x, y, data, order='xy')
        title = 'data(x,y) = sin(2*y)*exp(-y**2/s**2)*cos(x)'

    else:
        data = np.zeros((len(y), len(x)))
        for i in range(len(y)):
            yy = y[i]
            for j in range(len(x)):
                xx = x[j]
                # data = data(y,x)
                data[i,j] = np.sin(2*xx)*np.exp(-xx**2/(0.75*np.pi)**2)*np.cos(yy)

        im = plotting.plot2d(x, y, data, order='yx')
        title = 'data(y,x) = sin(2*x)*exp(-x**2/s**2)*cos(y)'

    # add plot attributes
    cb = plt.colorbar(im, orientation='vertical')
    plt.xlabel('x = [-2*pi, 2*pi]'); plt.ylabel('y = [-pi, pi]'); plt.title(title)
    plt.xlim(np.amin(x), np.amax(x))
    plt.ylim(np.amin(y), np.amax(y))

    if (save):
        plt.savefig("plot2d_test_image.png")
        print("saved image to plot2d_test_image.png")
    else:
        plt.show()

    ############################
    # test the get_color method
    print("\nTesting the get_color module...")
    cmap = 'jet'
    x = np.arange(0., 2*np.pi, 2*np.pi/N)
    freqs = np.array([1., 2., 3., 4., 5.]); fmin=np.amin(freqs); fmax=np.amax(freqs)
    plt.clf()
    for f in freqs:
        c = plotting.get_color(f, xmin=fmin, xmax=fmax)
        plt.plot(x, np.sin(f*x), color=c, linestyle='-')

    # set a colorbar, but need a scalar mappable first, also need empty fake data
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=fmin, vmax=fmax))
    sm._A = []
    cbar = plt.colorbar(sm, orientation='vertical')
    cbar.set_label(r"frequency $f$")

    plt.xlabel(r'$t$'); plt.ylabel(r'$y = \sin (2\pi f t)$')

    plt.xlim(0, 2*np.pi); plt.ylim(-1.1, 1.1)
    plt.tight_layout()

    if (save):
        plt.savefig("test_get_color.png")
        print("saved image to test_get_color.png")
    else:
        plt.show()

    ############################
    # test the shifted color map
    print("\nTesting the MidPointNorm module...")
    cmap = 'RdBu_r'
    X = np.random.random_integers(low=-15, high=5, size=(64,64))

    plt.clf()
    plt.subplot(221)
    norm = plotting.MidPointNorm()
    plt.imshow(X, norm=norm, cmap=cmap)
    plt.colorbar()
    plt.title("default call")

    plt.subplot(222)
    norm = plotting.MidPointNorm(midpoint=-10)
    plt.imshow(X, norm=norm, cmap=cmap)
    plt.colorbar()
    plt.title("midpoint = -10")

    plt.subplot(223)
    norm = plotting.MidPointNorm(midpoint=1)
    plt.imshow(X, norm=norm, cmap=cmap)
    plt.colorbar()
    plt.title("midpoint = 1")

    plt.subplot(224)
    norm = plotting.MidPointNorm(midpoint=-5)
    plt.imshow(X, norm=norm, cmap=cmap)
    plt.colorbar()
    plt.title("midpoint = -5")

    plt.tight_layout()

    if (save):
        plt.savefig("test_midpoint_cmap.png")
        print("saved image to test_midpoint_cmap.png")
    else:
        plt.show()
    print()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

