"""
Module to test the various filesystem routines

Usage:
    test_file_io.py [options]

Options:
    --dir=<d>      Directory to tree walk [default: ./]
    --include=<i>  Comma separated list of file extensions to include
    --exclude=<d>  Comma separated list of directories to exclude
"""

from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from NumericalTools import file_io

# import any other routines
import numpy as np

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    top_dir = args.pop('--dir', './')
    include_ext = args.pop('--include', None)
    exclude_dirs = args.pop('--exclude', None)

    if (include_ext is None):
        include_ext = []
    else:
        include_ext = include_ext.split(",")
    if (exclude_dirs is None):
        exclude_dirs = []
    else:
        exclude_dirs = exclude_dirs.split(",")

    # report
    print("\nInputs:")
    print("\ttop directory = {}".format(top_dir))
    print("\tinclude-file-extensions = {}".format(include_ext))
    print("\texclude-directories = {}".format(exclude_dirs))

    files = file_io.treewalk(top_dir, include_ext=include_ext, exclude_dirs=exclude_dirs)

    print("\nResults:")
    print("\tFound the following files")
    for i, f in enumerate(files):
        print("\t\t{}, {}".format(i+1, f))
    print()

    array = np.random.randn(10, 8, 3)
    scalar = 6.75
    string = "hello"
    savefile = "save_read_data_test.dat"
    print("Saving data to file = {}".format(savefile))
    try:
        file_io.save_data(savefile, array=array, scalar=scalar, string=string)
        print("...success")
    except:
        print("...FAILED")

    print("\nReading data from file = {}".format(savefile))
    try:
        data, data_keys = file_io.read_data(savefile)
        array = data['array']
        scalar = data['scalar']
        string = data['string']
        print("...success")
        print("\tfound data keys:")
        for k in data_keys:
            print("\t\t{}".format(k))
    except:
        print("...FAILED")
    print()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

