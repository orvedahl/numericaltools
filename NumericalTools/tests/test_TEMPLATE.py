"""
Module to test the various TEMPLATE routines

Usage:
    test_TEMPLATE.py [options]

Options:
    --debug     Run in debug mode [default: False]
"""

from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from NumericalTools import ...

# import any other routines
import ...

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    # unpack command line options
    debug = args.pop('--debug', False)

    # set up the test

    # call the NumericalTools routine

    # process results
    print("you are using debug = {}".format(debug))

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

