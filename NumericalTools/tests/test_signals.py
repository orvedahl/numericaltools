"""
Module to test the various Signal routines

Usage:
    test_signals.py [options]

Options:
    --domain=<d>      Comma separated domain [default: -2,4]
    --grid=<g>        Specify grid type: uniform, random [default: random]
    --N=<n>           Number of grid points [default: 128]
    --window=<w>      One of: flat, hann, hamming, bartlett, blackman [default: hann]
    --win-length=<l>  Length of window, default is 0.01*len(data)
    --save            Save plot [default: False]
"""

from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from testing_utilities import get_error, get_function
from NumericalTools import signals
from NumericalTools.misc import grids

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    N = int(args.pop('--N', '128'))
    domain = args.pop('--domain', '-2,4')
    window = args.pop('--window', 'hann')
    window_length = args.pop('--win-length', None)
    saveplot = args.pop('--save', False)
    grid = args.pop('--grid', 'random')

    if (window_length is not None):
        window_length = int(window_length)
    xlo, xhi = float(domain.split(",")[0]), float(domain.split(",")[1])

    if (grid == 'uniform'):
        G = grids.UniformGrid(N, a=xlo, b=xhi)
        x = G.x
    elif (grid == 'random'):
        G = grids.RandomGrid(N, a=xlo, b=xhi, seed=43)
        x = G.x
    else:
        print("\n---ERROR: grid not recognized = {}\n".format(grid))
        return

    funcs = ['sin', 'gauss']
    sigs  = [ 1.1,   0.20]

    nplots = len(funcs); plot_ind = int(100*nplots + 11)

    plt.clf()
    for i, func in enumerate(funcs):
        # get function and add noise to it
        f, dfdx, I, fname = get_function(func); sigma = sigs[i]
        fx = f(x) + np.random.randn(len(x))*sigma

        # get the smoothed function
        fx_smooth = signals.smooth(fx, window_length=window_length, window=window)

        print("\tshape of x, fx, smoothed-fx = {}, {}, {}".format(
                 np.shape(x), np.shape(fx), np.shape(fx_smooth)))

        iplot = plot_ind + i
        plt.subplot(iplot)
        if (i==0):
            plt.title("signal.smooth, N = {}, window = {}, wlength = {}".format(\
                          N, window, window_length))
        plt.plot(x, fx,        label='F+noise', marker='x', linestyle=':', color='r')
        plt.plot(x, fx_smooth, label='Smooth', marker='', linestyle='-', color='b')
        plt.plot(x, f(x),      label='F(x) orig', marker='', linestyle='--', color='k')
        plt.legend(loc='best'); plt.ylabel("{}".format(func))
    plt.xlabel('x')

    # report
    print("\nInputs:")
    print("\tdomain = {}".format(domain))
    print("\tgrid = {}".format(grid))
    print("\tN = {}".format(N))
    print("\twindow = {}".format(window))
    print("\twindow length = {}".format(window_length))

    if (saveplot):
        output = "test_signals.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

