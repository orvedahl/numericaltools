"""
module to hold analytic test functions and other routines useful for unit tests

Methods:
    returned arguments such as f(x) indicate it is callable. possible
    optional arguments are not shown.

    --absolute & relative error
        abs, rel = get_error(x1, x2)

    --analytic functions and their derivative & antiderivative
        f(x), dfdx(x), I(x), fname = def get_function(func)

    --analytic functions and their FFTs
        f(x), fft(x), fname = get_function_FFT(func)

"""

from __future__ import print_function
from sys import exit
import numpy as np

def help():
    print(__doc__)

def get_function_FFT(func):
    """
    return function, and its FFT
    """

    supported = ['cos', 'cos2', 'cos3']

    atol =1e-3; rtol=1e-3
    func = func.lower()
    if (func not in supported):
        print("\n---ERROR: unknown function = {}".format(func))
        print("\tsupported funcs = {}".format(supported))
        exit()

    if (func == 'cos'):
        # np.isclose works better as a "delta" function then np.where
        strname = "4*cos(x)"
        def f(x):
            return 4*np.cos(x)
        def fft(f):
            ft = np.zeros_like(f)
            ind = np.isclose(f, 0.5/np.pi*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 4.
            return ft

    elif (func == 'cos2'):
        strname = "3*cos(2*pi*5*x) + 5*cos(2*pi*x)"
        def f(x):
            return 3.*np.cos(2*np.pi*5*x)+ 5.*np.cos(2*np.pi*x)
        def fft(f):
            ft = np.zeros_like(f)
            ind = np.isclose(f, 5.*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 3.
            ind = np.isclose(f, 1.0*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 5.
            return ft

    elif (func == 'cos3'):
        strname = "10*cos(2*pi*x) + 3*cos(2*pi*2*x) + 5*cos(2*pi*3*x) + 7*cos(2*pi*7*x)"
        def f(x):
            return 10.*np.cos(2*np.pi*x) + 3.*np.cos(4*np.pi*x) \
                  + 5.*np.cos(6*np.pi*x) + 7.*np.cos(14*np.pi*x)
        def fft(f):
            ft = np.zeros_like(f)
            ind = np.isclose(f, 1.*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 10.
            ind = np.isclose(f, 2.*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 3.
            ind = np.isclose(f, 3.*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 5.
            ind = np.isclose(f, 7.*np.ones_like(f), atol=atol, rtol=rtol)
            ft[ind] = 7.
            return ft

    return f, fft, strname

def get_function(func, return_funcs=False):
    """
    return function, derivative, and antiderivative
    """

    supported = ['sin', 'x2', 'gauss', 'cos', 'exp', 'log', 'x22']

    func = func.lower()
    if (func not in supported):
        print("\n---ERROR: unknown function = {}".format(func))
        print("\tsupported funcs = {}".format(supported))
        exit()

    if (func == 'sin'):
        strname = "3*sin(pi*x - 1.2) + 2"
        def f(x):
            return 3.*np.sin(np.pi*x-1.2) + 2.
        def dfdx(x):
            return 3.*np.pi*np.cos(np.pi*x-1.2)
        def I(x):
            return -3./np.pi*np.cos(np.pi*x-1.2) + 2.*x

    elif (func == 'x2'):
        strname = "3*x**2 - 4*x + pi"
        def f(x):
            return 3.*x**2 - 4.*x + np.pi
        def dfdx(x):
            return 6.*x - 4.
        def I(x):
            return x**3 - 2.*x**2 + np.pi*x

    elif (func == 'gauss'):
        from scipy.special import erf
        strname = "2/sqrt(pi)*exp(-x**2)"
        def f(x):
            return 2./np.sqrt(np.pi)*np.exp(-x**2)
        def dfdx(x):
            return -2.*x*f(x)
        def I(x):
            return erf(x)

    elif (func == 'cos'):
        strname = "cos(x)"
        def f(x):
            return np.cos(x)
        def dfdx(x):
            return -np.sin(x)
        def I(x):
            return np.sin(x)

    elif (func == 'exp'):
        strname = "3*exp(-2*x)"
        def f(x):
            return 3.*np.exp(-2.*x)
        def dfdx(x):
            return -2.*f(x)
        def I(x):
            return -0.5*f(x)

    elif (func == 'log'):
        strname = "log(1 + x)"
        def f(x):
            return np.log(1.+x)
        def dfdx(x):
            return 1./(1.+x)
        def I(x):
            return (1.+x)*f(x) - x

    elif (func == 'x22'):
        strname = "x + x**2"
        def f(x):
            return x + x**2
        def dfdx(x):
            return 1. + 2.*x
        def I(x):
            return x**2/2. + x**3/3.

    if (return_funcs):
        return f, dfdx, I, strname, supported
    else:
        return f, dfdx, I, strname

def get_error(x1, x2, zero_tol=1e-16):
    """
    calculate absolute and relative errors
        abs = (x1 - x2)
        rel = abs/x2
    """
    # make sure x2 minimum is zero_tol
    # this avoids dividing by zero issues
    if (len(np.shape(x2)) > 0):
        # x2 is array
        ind = np.where(np.abs(x2) < zero_tol)
        x2[ind] = zero_tol
    else:
        # x2 is scalar
        if (abs(x2) < zero_tol):
            x2 = zero_tol

    a = x1-x2 # absolute error

    # make sure abs error min is zero_tol
    # this avoids zeros when log plotting errors
    if (len(np.shape(a)) > 0):
        ind = np.where(np.abs(a) < zero_tol)
        a[ind] = zero_tol
    else:
        if (abs(a) < zero_tol):
            a = zero_tol

    r = a/x2 # relative error

    return a,r

