"""
Module to test various Interpolation operations

Usage:
    test_interp.py [options]

Options:
    --debug        Debug mode [default: False]
    --domain=<d>   Comma separated list giving xlo,xhi [default: -2,4]
    --grid=<g>     Specify grid, uniform, random, cheb [default: cheb]
    --extrema      Use extrema grid [default: False]
    --high-res     Include high resolutions in convergence test [default: False]
    --save         Save plot, do not display it [default: False]
    --verbose      Print more information to screen [default: False]
"""
from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from testing_utilities import get_error, get_function
from NumericalTools import interp
from NumericalTools.misc import grids

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    debug = args.pop('--debug', False)
    high_res = args.pop('--high-res', False)
    grid = args.pop('--grid', 'cheb')
    extrema = args.pop('--extrema', False)
    saveplot = args.pop('--save', False)
    verbose = args.pop('--verbose', False)
    domain = args.pop('--domain', '-2,4')

    if (high_res):
        Ns = [9, 16, 32, 64, 128, 256, 512, 1024, 2048]
    else:
        Ns = [9, 16, 32, 64, 128, 256]

    domain = [float(domain.split(",")[0]), float(domain.split(",")[1])]
    xlo, xhi = domain

    methods = ['rbf', 'linear', 'quadratic', 'cubic']
    funcs = ['sin', 'gauss', 'exp']

    zero_tol = 1e-16
    max_tol = 1e50

    errors = {} # hold all errors
    for func in funcs:
        if (verbose):
            print("starting func = {}".format(func))
        f, dfdx, _I, funcname = get_function(func)

        err = {}
        for i in range(len(methods)):
            method = methods[i]

            i_error = []
            for n in Ns:
                if (verbose):
                    print("\tworking on N = {}".format(n))
                if (grid == 'cheb'):
                    G = grids.ChebGrid(n, a=xlo, b=xhi, zeros=not(extrema))
                    x = G.xab
                elif (grid == 'uniform'):
                    G = grids.UniformGrid(n, a=xlo, b=xhi, endpoints=True)
                    x = G.x
                else:
                    print("\n---ERROR: grid not recognized = {}\n".format(grid))
                    return

                fx = f(x) # establish the grided data

                interp_f = interp.interp(fx, x, method=method)

                # evaluate the true func and the interpolant
                G = grids.Random(int(0.5*n), a=xlo, b=xhi, seed=43)
                xeval = G.x
                ftrue = f(xeval); finterp = interp_f(xeval)

                # get L2 error
                dx = np.mean(xeval[1:] - xeval[:-1])
                L2 = np.sqrt(dx*np.sum((ftrue - finterp)**2))
                if (L2 > max_tol):
                    L2 = max_tol
                elif (L2 < zero_tol):
                    L2 = zero_tol
                i_error.append(L2)
                if (verbose):
                    print("\t\tdifferentiation L2 error = {}".format(L2))

            # convert to arrays
            i_error = np.array(i_error)

            err[method] = [1.*np.array(Ns), i_error]

        errors[func] = err

    # report
    print("\nInputs:")
    print("\tdomain = {}".format(domain))
    print("\tgrid = {}".format(grid))
    print("\textrema = {}".format(extrema))

    # plots
    plt.clf()
    N = errors[funcs[0]][methods[0]][0]; ind = 0
    e = errors[funcs[0]][methods[0]][1]
    if (debug):
        plt.plot(N, e[ind]*(N/N[ind])**-3,label=r'$O(N^{-3})$', linestyle='--',color='k')
        plt.plot(N, e[ind]*(N/N[ind])**-4,label=r'$O(N^{-4})$', linestyle='--',color='k')
        plt.plot(N, e[ind]*(N/N[ind])**-5,label=r'$O(N^{-5})$', linestyle='--',color='k')
    for key in errors.keys():
        for m in errors[key].keys():
            N    = errors[key][m][0]
            err = errors[key][m][1]
            plt.plot(N, err, label=m+', '+key, marker='x', linestyle=':')

    plt.title("Interpolation Convergence")
    plt.legend(loc='upper right', ncol=1)
    plt.xlabel('N'); plt.ylabel('error')
    plt.xscale('log'); plt.yscale('log')
    plt.xlim(0.5*Ns[0], 15*Ns[-1])

    if (saveplot):
        output = "test_interpolation.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()
    print()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

