"""
Module to test the various FFT routines

Usage:
    test_fft.py [options]

Options:
    --debug         Run in debug mode [default: False]
    --domain=<d>    Comma separated list giving xmin & xmax [default: -2,4]
    --grid=<grid>   What kind of grid: cheb, uniform [default: uniform]
    --N=<n>         Specify number of grid points [default: 128]
    --window=<w>    What window to use: none, hanning, blackman-harris [default: none]
    --test-2d       Test the FFT on single axis of 2D array [default: False]
    --dct           Test the DCT [default: False]
    --plot-power    Plot the power spectra [default: False]
    --save          Save plot, do not display it [default: False]
"""

from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from NumericalTools import fft
from NumericalTools.misc import grids

# import any other routines
from testing_utilities import get_function_FFT, get_error
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    debug = args.pop('--debug', False)
    grid = args.pop('--grid', 'uniform')
    N = int(args.pop('--N', '128'))
    window = args.pop('--window', 'none')
    saveplot = args.pop('--save', False)
    domain = args.pop('--domain', '-2,4')
    test_2d = args.pop('--test-2d', False)
    test_dct = args.pop('--dct', False)
    plot_power = args.pop('--plot-power', False)

    xlo, xhi = float(domain.split(",")[0]), float(domain.split(",")[1])

    if (grid == 'cheb'):
        G = grids.ChebGrid(N, zeros=True)
        x = G.x
    else:
        G = grids.UniformGrid(N, a=xlo, b=xhi)
        x = G.x

    # get 1D function, f
    funcs = ['cos3']

    for func in funcs:
        f, fft_true, fname = get_function_FFT(func)
        print("\tworking on func = {}, f(x) = {}".format(func, fname))

        fx = f(x) # gridded data, make sure its a uniform grid
        if (grid == 'cheb'):
            fx, x = fft.interp_uniform(fx, x)

        print("\t\tFFT...")
        fk, freq, power, xwin = fft.FFT(fx, x, window='none', angular=False)
        fk_inv = fft.iFFT(fk)

        P_true = fft_true(freq)*np.conj(fft_true(freq))

        abs_errX, rel_errX = get_error(fx, fk_inv)
        abs_errK, rel_errK = get_error(fk, fft_true(freq))
        abs_errP, rel_errP = get_error(power, P_true)

        if (plot_power):
            plot_ind = 310
        else:
            plot_ind = 210
        plt.clf()
        plt.subplot(plot_ind + 1)
        plt.title("FFT")
        plt.plot(x, fx,     color='r', label=r'$F(t)$')
        plt.plot(x, fk_inv, color='b', label=r'$\mathcal{F}^{-1}\mathcal{F}[F]$')
        plt.plot(x, xwin,   color='g', label='window', linestyle=':')
        plt.legend(loc='best')
        plt.xlabel('t'); plt.ylabel('Time Space')
        plt.twinx()
        plt.plot(x, np.abs(abs_errX), color='k', linestyle='--')
        #plt.plot(x, np.abs(rel_errX), color='k', linestyle=':')
        plt.ylabel('Errors'); plt.yscale('log')
        plt.subplot(plot_ind + 2)
        plt.plot(freq, fk,             color='r', label=r'$\mathcal{F}[F](f)$')
        plt.plot(freq, fft_true(freq), color='b', label='Analytic')
        plt.xlabel(r'$f$'); plt.ylabel('Freq Space')
        plt.legend(loc='best')
        plt.twinx()
        plt.plot(freq, np.abs(abs_errK), color='k', linestyle='--')
        #plt.plot(freq, np.abs(rel_errK), color='k', linestyle=':')
        plt.ylabel('Errors'); plt.yscale('log')
        if (plot_power):
            plt.subplot(plot_ind + 3)
            plt.plot(freq, power,  color='r', label=r'$|\mathcal{F}|^2(f)$')
            plt.plot(freq, P_true, color='b', label=r'Analytic')
            plt.xlabel(r'$f$'); plt.ylabel('Power')
            plt.legend(loc='best')
            plt.twinx()
            plt.plot(freq, np.abs(abs_errP), color='k', linestyle='--')
            #plt.plot(freq, np.abs(rel_errP), color='k', linestyle=':')
            plt.ylabel('Errors'); plt.yscale('log')
        if (saveplot):
            output = "test_FFT.png"
            plt.savefig(output)
            print("\nsaved image = {}".format(output))
        else:
            plt.show()

        if (test_dct):
            print("\t\tDCT...")
            norm = None # 'ortho'
            fk = fft.DCT(fx, x=x, window='none', norm=norm)
            fk_inv = fft.iDCT(fk, norm=norm)

            abs_errX, rel_errX = get_error(fx, fk_inv)

            plt.clf()
            plt.title("DCT")
            plt.plot(x, fx,     color='r', label=r'$F(t)$')
            plt.plot(x, fk_inv, color='b', label=r'$\mathcal{D}^{-1}\mathcal{D}[F]$')
            plt.plot(x, xwin,   color='g', label='window', linestyle=':')
            plt.legend(loc='best')
            plt.xlabel('t'); plt.ylabel('Time Space')
            plt.twinx()
            plt.plot(x, np.abs(abs_errX), color='k', linestyle='--')
            #plt.plot(x, np.abs(rel_errX), color='k', linestyle=':')
            plt.ylabel('Errors'); plt.yscale('log')
            if (saveplot):
                output = "test_DCT.png"
                plt.savefig(output)
                print("\nsaved image = {}".format(output))
            else:
                plt.show()

        if (test_2d):
            # get 2D function, f2d
            F = np.zeros((2, len(x)))
            F[0,:] = fx
            F[1,:] = 5*fx

            print("\t\tFFT on first axis of 2D array...")
            fk, freq, power, xwin = fft.FFT(F, x, window='none', axis=1, angular=False)

            abs_errK0, rel_errK0 = get_error(fk[0,:], fft_true(freq))
            abs_errK1, rel_errK1 = get_error(fk[1,:], fft_true(freq))

            plt.clf()
            plt.title("FFT first axis of 2D array")
            plt.plot(freq, fk[0,:], color='r', label=r'$\mathcal{F}[F(0,t)](f)$')
            plt.plot(freq, fft_true(freq), color='b', label='Analytic')
            plt.plot(freq, fk[1,:], color='r', label=r'$\mathcal{F}[F(1,t)](f)$', linestyle='--')
            plt.plot(freq, 5*fft_true(freq), color='b', label='Analytic', linestyle='--')
            plt.xlabel(r'$f$'); plt.ylabel('Freq Space')
            plt.legend(loc='best')
            plt.twinx()
            plt.plot(freq, np.abs(abs_errK0), color='k', linestyle='--')
            #plt.plot(freq, np.abs(rel_errK0), color='k', linestyle=':')
            plt.plot(freq, np.abs(abs_errK1), color='k', linestyle='--')
            #plt.plot(freq, np.abs(rel_errK1), color='k', linestyle=':')
            plt.ylabel('Errors'); plt.yscale('log')
            if (saveplot):
                output = "test_FFT_single_axis0.png"
                plt.savefig(output)
                print("\nsaved image = {}".format(output))
            else:
                plt.show()

            F = np.zeros((len(x), 2))
            F[:,0] = fx
            F[:,1] = 5*fx

            print("\t\tFFT on second axis of 2D array...")
            fk, freq, power, xwin = fft.FFT(F, x, window='none', axis=0, angular=False)

            abs_errK0, rel_errK0 = get_error(fk[:,0], fft_true(freq))
            abs_errK1, rel_errK1 = get_error(fk[:,1], fft_true(freq))

            plt.clf()
            plt.title("FFT second axis of 2D array")
            plt.plot(freq, fk[:,0], color='r', label=r'$\mathcal{F}[F(t,0)](f)$')
            plt.plot(freq, fft_true(freq), color='b', label='Analytic')
            plt.plot(freq, fk[:,1], color='r', label=r'$\mathcal{F}[F(t,1)](f)$', linestyle='--')
            plt.plot(freq, 5*fft_true(freq), color='b', label='Analytic', linestyle='--')
            plt.xlabel(r'$f$'); plt.ylabel('Freq Space')
            plt.legend(loc='best')
            plt.twinx()
            plt.plot(freq, np.abs(abs_errK0), color='k', linestyle='--')
            #plt.plot(freq, np.abs(rel_errK0), color='k', linestyle=':')
            plt.plot(freq, np.abs(abs_errK1), color='k', linestyle='--')
            #plt.plot(freq, np.abs(rel_errK1), color='k', linestyle=':')
            plt.ylabel('Errors'); plt.yscale('log')
            if (saveplot):
                output = "test_FFT_single_axis1.png"
                plt.savefig(output)
                print("\nsaved image = {}".format(output))
            else:
                plt.show()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

