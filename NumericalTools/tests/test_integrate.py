"""
Module to test the various Integration operations

Usage:
    test_integrate.py [options]

Options:
    --debug         Debug mode [default: False]
    --domain=<d>    Comma separated list giving xmin & xmax [default: -2,4]
    --grid=<grid>   What kind of grid: cheb, uniform [default: cheb]
    --extrema       With a chebyshev grid, use the extrema of T_N [default: False]
    --endpoints     Use zeros grid and include endpoints [default: False]
    --high-res      Do convergence with high resolutions [default: False]
    --save          Save plot, do not display it [default: False]
    --verbose       Print more information to screen [default: False]
"""
from __future__ import print_function

# if necessary modify pythonpath, allows running test routines
# even if NumericalTools has not been installed and added to the pythonpath
try:
    import NumericalTools
except ImportError:
    print("\n---WARNING: NumericalTools not in PYTHONPATH. modifying it so test will run\n")
    import env

# import the needed NumericalTools routine(s)
from testing_utilities import get_error, get_function
from NumericalTools import integrate
from NumericalTools.misc import grids

# import any other routines
import numpy as np
import matplotlib.pyplot as plt

def help():
    print(__doc__)

def run(**args):
    """
    args is a dictionary similar to docopt. for example:
        debug option is passed by defining args={'--debug':True}
        followed by calling run(**args)
    """

    debug = args.pop('--debug', False)
    grid = args.pop('--grid', "cheb")
    extrema = args.pop('--extrema', False)
    endpoints = args.pop('--endpoints', False)
    high_res = args.pop('--high-res', False)
    saveplot = args.pop('--save', False)
    verbose = args.pop('--verbose', False)
    domain = args.pop('--domain', '-2,4')

    if (high_res):
        Ns = [9, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192]
    else:
        Ns = [9, 16, 32, 64, 128, 256, 512, 1024]

    domain = [float(domain.split(",")[0]), float(domain.split(",")[1])]
    xlo, xhi = domain

    if (grid == 'cheb'):
        methods = ['cheb', 'trap']
    else:
        methods = ['simps', 'trap']
    funcs = ['gauss', 'exp', 'sin']

    if (debug):
        methods = ['8th']; funcs = ['gauss', 'exp', 'sin']

    zero_tol = 1e-16
    max_tol = 1e50

    errors = {} # hold all errors
    for func in funcs:
        if (verbose):
            print("starting func = {}".format(func))
        f, dfdx, _I, fname = get_function(func)

        def I(x):
            return _I(x)-_I(xlo)
        def_exact = I(xhi) - I(xlo)

        err = {}
        for i in range(len(methods)):
            method = methods[i]

            def_int_err = []; indef_err = []
            for n in Ns:
                if (verbose):
                    print("\tworking on N = {}".format(n))
                if (grid == 'cheb'):
                    G = grids.ChebGrid(n, a=xlo, b=xhi, zeros=not(extrema),
                                       endpoints=endpoints)
                    x = G.xab
                elif (grid == 'uniform'):
                    G = grids.UniformGrid(n, a=xlo, b=xhi, endpoints=True)
                    x = G.x
                elif (grid == 'random'):
                    G = grids.RandomGrid(n, a=xlo, b=xhi, seed=43)
                    x = G.x
                else:
                    print("\n---ERROR: unrecognized grid = {}\n".format(grid))
                    return

                fx = f(x); indef_exact = I(x)

                # definite integral
                defI = integrate.integrate(fx, x, method=method, zeros=not(extrema),
                                           endpoints=endpoints)

                # indefinite integral
                indefI = integrate.integrate(fx, x, method=method, zeros=not(extrema),
                                             indefinite=True, endpoints=endpoints)

                # calculate errors
                abs_err, rel_err = get_error(defI, def_exact, zero_tol=zero_tol)
                def_abs_err = np.abs(abs_err)
                def_rel_err = np.abs(rel_err)
                def_int_err.append(def_rel_err)

                # L2
                dx = np.mean(x[1:] - x[:-1])
                L2 = np.sqrt(dx*np.sum((indefI - I(x))**2))
                if (L2 > max_tol):
                    L2 = max_tol
                elif (L2 < zero_tol):
                    L2 = zero_tol
                indef_err.append(L2)
                if (verbose):
                    print("\t\tindefinite integrate L2 error = {}".format(L2))

            # convert to arrays
            err[method] = [1.*np.array(Ns), np.array(def_int_err), np.array(indef_err)]

        errors[func] = err

    # report
    print("\nInputs:")
    print("\tdomain : {}".format(domain))
    print("\tgrid : {}".format(grid))
    print("\textrema : {}".format(extrema))

    # plots
    plt.clf()
    N = errors[funcs[0]][methods[0]][0]; ind = -1
    e = errors[funcs[0]][methods[0]][1]
    if (grid == 'uniform'):
        plt.plot(N, e[ind]*(N/N[ind])**-2, label=r'$O(N^{-2})$', linestyle='-.', color='k')
        plt.plot(N, e[ind]*(N/N[ind])**-4, label=r'$O(N^{-4})$', linestyle='--', color='k')
        if ('8th' in methods):
            plt.plot(N, e[ind]*(N/N[ind])**-8, label=r'$O(N^{-.})$', linestyle='--', color='k')
    else:
        plt.plot(N, e[ind]*(N/N[ind])**-2, label=r'$O(N^{-2})$', linestyle='--', color='k')
    for f in errors.keys():
        for m in errors[f].keys():
            N      = errors[f][m][0]
            defI   = errors[f][m][1]
            indefI = errors[f][m][2]
            plt.plot(N, defI,   label=m+', '+f+', def', marker='x', linestyle=':')
            plt.plot(N, indefI, label=m+', '+f+r', $I(x)$', marker='x', linestyle=':')

    plt.title("Integrate Convergence")
    plt.xlabel('N'); plt.ylabel('error')
    plt.legend(loc='upper right', ncol=1)
    plt.xscale('log'); plt.yscale('log')
    plt.xlim(0.5*Ns[0], 100*Ns[-1])

    if (saveplot):
        output = "test_integrate.png"
        plt.savefig(output)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()
    print()

if __name__ == "__main__":
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)

