"""
Module to hold various routines used by all polynomial types
"""

from __future__ import print_function
import numpy as np
from sys import exit

def clenshaw(x, c, func='Tn'):
    """
    evaluate the sum at the given x,
       f(x) = sum_0^n c_k * F_k(x)
    using the expansion coefficients c_k and the known
    three term recurrence relation for F_k(x). the keyword
    "func" determines which basis functions are used

    --x is array like that specifies evaluation points
    --c is array of coefficients

    Clenshaw Recurrence:
       1) define function expansion
            f(x) = sum_{0}^{N} c_k * F_k(x)

       2) define known recurrence relation
            F_{n+1} = alpha(n,x)*F_{n} + beta(n,x)*F_{n-1}

            Cheb 1st kind, T_n    : alpha = 2*x, beta = -1
            Cheb 2nd kind, U_n    : alpha = 2*x, beta = -1
            Legendre,      P_n    : alpha = (2*n+1)*x/(n+1), beta = -n/(n+1)
            d(Legendre)/dx dP_n/dx: alpha = (2*n+1)*x/n, beta = -(n+1)/n

       3) define
            a) y_{N+2} = y_{N+1} = 0
            b) y_k = alpha(k,x)*y_{k+1} + beta(k+1,x)*y_{k+2} + c_{k}

       4) backsolve and plug in to get final sum (lots of stuff cancels)
            f(x) = c_{0}*F_{0}(x) + y_{1}*F_{1}(x) + beta(1,x)*F_{0}(x)*y_{2}
            note: y_{0} is not used

            Cheb 1st kind, T_n: F_0 = 1, F_1 = x
            Cheb 2nd kind, U_n: F_0 = 1, F_1 = 2*x
            Legendre,      P_n: F_0 = 1, F_1 = x
    """
    func = func.lower()
    supported = ["tn", "un", "pn", "dpndx"]
    if (func not in supported):
        print("\n---ERROR: clenshaw, function not supported, func = {}".format(func))
        print("\tsupported funcs = {}".format(supported))
        exit()

    x = np.asarray(x); N = len(c)

    # define alpha, beta, and first two basis functions
    if (func == 'tn'):
        # Chebyshev of 1st kind
        def get_alpha(n, x):
            return 2.*x
        def get_beta(n, x):
            return -1.0
        F0 = 1.0; F1 = x

    elif (func == 'un'):
        # Chebyshev of 2nd kind
        def get_alpha(n, x):
            return 2.*x
        def get_beta(n, x):
            return -1.0
        F0 = 1.0; F1 = 2.*x

    elif (func == 'pn'):
        # Legendre
        def get_alpha(n, x):
            return (2.*n+1.)*x/(n+1.)
        def get_beta(n, x):
            return -n/(n+1.)
        F0 = 1.0; F1 = x

    elif (func == 'dpndx'):
        # 1st derivative of Legendre
        def get_alpha(n, x):
            return (2.*n+1.)*x/float(n)
        def get_beta(n, x):
            return -(n+1.)/float(n)
        F0 = 0.0; F1 = 1.0

    bkp1 = bkp2 = 0.               # initialize: b_{k+1} = b_{k+2} = 0
    for k in range(N-1, 0, -1):    # loop from N-1 --> 1, inclusive
        alpha = get_alpha(k,x)     # evaluate alpha/beta at k & x
        beta  = get_beta(k+1,x)       
        bk = c[k] + alpha*bkp1 + beta*bkp2 # recursion
        bkp2 = bkp1                # after loop, b[2] will be stored in bkp2
        bkp1 = bk                  # after loop, b[1] will be stored in bkp1

    # evaluate sum using beta(k=1,x), b_{1}, and b_{2}. b_{0} is not used
    beta = get_beta(1, x)
    fx = F0*c[0] + F1*bkp1 + beta*F0*bkp2

    return fx

