"""
Module to handle various Legendre operations

    P_0 = 1
    P_1 = x

    (n+1)*P_{n+1} = (2*n+1)*x*P_{n} - n*P_{n-1}
"""

from __future__ import print_function
import numpy as np
from .common import clenshaw
from ..misc import grids

class Legendre():

    def __init__(self, f, a=-1.0, b=1.0, endpoints=False):

        self.a = a   # lower boundary
        self.b = b   # upper boundary

        self.fx = f        # function value at grid points, increasing array indices
        self.Num = len(f)  # are assumed to correspond to increasing grid values
                           # so if the function is f(x) = x then the ordering is
                           # assumed to be f_0 < f_1 < ... < f_i < ... < f_n-2 < f_n-1

        self.endpoints = endpoints

        # generate the grid
        self.grid = grids.LegendreGrid(self.Num, a=self.a, b=self.b,
                                       endpoints=self.endpoints)

        self.x = self.grid.x
        self.deriv_coeff = self.grid.deriv_coeff
        self.integ_coeff = self.grid.integ_coeff

        # get Legendre coefficients
        self.a_k = self._get_coeffs()

        # storage
        self.dfdx = None            # first derivative coefficients
        self.def_integral = None    # value of definite integral over whole domain
        self.Fx = None              # indefinite integral coefficients

    def _get_coeffs(self):
        """
        return Legendre coefficients a_k
           f(x) = sum a_k & P_k(x)

           a_k = (2*k+1)/2 * int_{-1}^{1} f(x)*P_k(x) dx
               = (2*k+1)/2 * sum_n w(x_n)*f(x_n)*P_k(x_n)
        """
        coeffs = np.zeros((self.Num))
        for l in range(self.Num):
            C = (2.*l + 1.)/2.
            Pl, dPldx = self.grid._eval_nth_legendre(self.x, l)

            coeffs[l] = C*np.sum(self.grid.weights*self.fx*Pl)

        return coeffs

    def _from_ab(self, r):
        return self.grid._from_ab(r)

    def _to_ab(self, x):
        return self.grid._to_ab(x)

    def deriv(self, x, return_instance=False):
        """
        derivative of Legendre series at x
          f(x)  = sum a_k * P_k(x)
          f'(x) = sum a_k * P'_k(x) = sum b_k * P_k(x)
          relate b_k to a_k using a three term recurrsion
          see Boyd's 2001 book equation A.41 of Appendix A
        """
        y = self._from_ab(x)
        C = self.deriv_coeff

        # ----OLD WAY: same error as using clenshaw with dPndx
        #if (self.dfdx is None):
        #    coeffs = np.zeros_like(self.a_k)
        #    N = len(self.a_k)
        #    for l in range(N):
        #        partial_sum = 0
        #        for p in range(l+1,N):
        #            if ((l+p)%2==1):
        #                partial_sum += self.a_k[p]
        #        coeffs[l] = (2.*l+1.)*partial_sum
        #    self.dfdx = coeffs
        # evaluate on supplied grid
        #dfdx  = C*clenshaw(y, self.dfdx, func='Pn')
        # ----OLD WAY----

        # evaluate on supplied grid
        dfdx  = C*clenshaw(y, self.a_k, func='dPndx')

        if (return_instance):
            return Legendre(dfdx, a=self.a, b=self.b, endpoints=self.endpoints)
        else:
            return dfdx

    def integ(self):
        """
        definite integral of Legendre series over whole domain
        """
        if (self.def_integral is not None):
            # save some work if it was already calculated
            return self.def_integral

        # use indefinite integral to calculate definite integral
        if (not self.endpoints):
            self.def_integral = self.integ_coeff*np.sum(self.fx*self.grid.weights)
        else:
            self.def_integral = self.antideriv(self.b) - self.antideriv(self.a)

        return self.def_integral

    def antideriv(self, x, return_instance=False):
        """
        indefinite integral of Legendre series
        """
        y = self._from_ab(x)
        C = self.integ_coeff

        if (self.Fx is None):
            coeffs = np.zeros_like(self.a_k)
            N = len(coeffs)
            for k in range(2,N-1):
                coeffs[k] = self.a_k[k-1]/(2.*k-1.) - self.a_k[k+1]/(2.*k+3.)
            coeffs[ 0 ] = -self.a_k[1]/3.
            coeffs[ 1 ] = self.a_k[0] - self.a_k[2]/5.
            coeffs[N-1] = self.a_k[N-2]/(2.*N-3.)

            # set F(a) = F(-1) = 0
            if (not self.endpoints):
                # using f(-1) = sum a_k * P_k(-1) = sum a_k * (-1)**k
                p = np.ones_like(coeffs)
                p[1::2] *= -1.0
                Const = -np.sum(p*coeffs)
            else:
                # using f(xlo) = sum a_k * P_k(xlo)
                Const = -clenshaw(self.grid.xlo, coeffs, func='Pn')
            coeffs[0] += Const

            self.Fx = coeffs

        Fx = C*clenshaw(y, self.Fx, func='Pn')

        if (return_instance):
            return Legendre(Fx, a=self.a, b=self.b, endpoints=self.endpoints)
        else:
            return Fx

    def __call__(self, x):
        """
        evaluate f(x) = sum_0^n a_k * P_k(x) at the given x points
        """
        y = self._from_ab(x)
        return clenshaw(y, self.a_k, func='Pn')

def antiderivative(fx, x, domain, endpoints=False):
    """
    indefinite integral of data on a legendre grid evaluated at given x pts
        F(x) = int_a^x f(t) dt
    constant chosen s.t. F(a) = 0
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    Fx = leg.antideriv(x)
    return Fx

def integral(fx, domain, endpoints=False):
    """
    definite integral of data on a legendre grid, returns single number
        I = int_a^b f(x) dx
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    I = leg.integ()
    return I

def derivative(fx, x, domain, endpoints=False):
    """
    1st deriv of data on a legendre grid evaluated at given x points
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    dfdx = leg.deriv(x)
    return dfdx

def is_grid_legendre(grid, domain, tol=5e-14):
    """
    determine if grid is a legendre grid. grid is assumed in [a,b]
    """
    # grid is assumed to be monotonically increasing
    if (grid[1] < grid[0]):
        grid = grid[::-1]

    N = len(grid); a = domain[0]; b = domain[1]

    # try traditional grid
    leg_grid = grids.LegendreGrid(N, a=a, b=b, endpoints=False)
    y = leg_grid.x
    x = leg_grid._from_ab(grid)

    # if difference between incoming grid and the legendre
    # grid is larger than the tolerance, then the grid is not legendre
    if (np.any(np.greater(np.abs(x-y), tol))):
        is_legendre_trad = False
    else:
        is_legendre_trad = True
    max_error_trad = np.amax(np.abs(x-y))

    # try endpoints=True grid
    leg_grid = grids.LegendreGrid(N, a=a, b=b, endpoints=True)
    y_end = leg_grid.x
    x_end = leg_grid._from_ab(grid)
    if (np.any(np.greater(np.abs(x_end-y_end), tol))):
        is_legendre_end = False
    else:
        is_legendre_end = True
    max_error_end = np.amax(np.abs(x_end-y_end))

    # a bunch of logic...
    if (is_legendre_trad or is_legendre_end):
        is_legendre = True
        if (is_legendre_trad):
            endpoints = False
        else:
            endpoints = True
    else:
        is_legendre = False
        endpoints = None

    results = {'legendre':is_legendre, 'endpoints':endpoints,
               'error':max_error_trad, 'endpoints-error':max_error_end}
    return results

