"""
Module to handle various Chebyshev operations

    1st Kind                        2nd Kind

    T_0 = 1                         U_0 = 1
    T_1 = x                         U_1 = 2*x

    T_{n+1} = 2*x*T_{n} - T_{n-1}   U_{n+1} = 2*x*U_{n} - U_{n-1}
"""

from __future__ import print_function
import numpy as np
from ..fft import fft
from .common import clenshaw
from ..misc import grids

class Chebyshev():

    def __init__(self, f, a=-1.0, b=1.0, zeros=True, endpoints=False):

        self.zeros = zeros  # use the Chebyshev zeros or extrema grid

        self.a = a   # lower boundary
        self.b = b   # upper boundary

        self.fx  = f      # function value at grid points, increasing array indices
        self.Num = len(f) # are assumed to correspond to increasing grid values
                          # so if the function is f(x) = x then the ordering is
                          # assumed to be f_0 < f_1 < ... < f_i < ... < f_n-2 < f_n-1

        # The grid may be a Chebyshev zeros grid and still have the endpoints
        # included. This means a non-standard linear transformation needs to be used
        if (not self.zeros):
            self.endpoints = False # this is only relevant for the zeros grid
        else:
            self.endpoints = endpoints

        # generate and store both grids
        self.grid = grids.ChebGrid(self.Num, a=self.a, b=self.b, zeros=self.zeros,
                               endpoints=self.endpoints)
        self.x = self.grid.x
        self.theta = self.grid.theta

        if (self.zeros):
            self.dct_type = 2
        else:
            self.dct_type = None

        # integration & differentiation coefficients
        self.deriv_coeff = self.grid.deriv_coeff
        self.integ_coeff = self.grid.integ_coeff

        # get Chebyshev coefficients
        self.a_k = self._get_coeffs()

        # storage
        self.dfdx = None            # first derivative coefficients
        self.def_integral = None    # value of definite integral over whole domain
        self.Fx = None              # indefinite integral coefficients

    def _get_coeffs(self):
        return cheb_coeffs(self.fx, zeros=self.zeros, dct_type=self.dct_type)

    def _from_ab(self, r):
        return self.grid._from_ab(r)

    def _to_ab(self, x):
        return self.grid._to_ab(x)

    def deriv(self, x, return_instance=False):
        """
        derivative of Chebyshev series at x
          f(x)  = sum a_k * T_k(x)
          f'(x) = sum a_k * T'_k(x) = sum b_k * T_k(x)
          relate b_k to a_k using a three term recurrsion
          see Boyd's 2001 book equation A.15 of Appendix A
          independent of node choice (zeros vs. extrema)
        """
        x = self._reverse_input(x) # reverse incoming x
        y = self._from_ab(x)        # convert x to [-1,1]
        C = self.deriv_coeff        # for converting back into [a,b] space

        if (self.dfdx is None):
            # done in spectral space:
            beta = np.zeros_like(self.a_k)
            N = len(beta)
            beta[N-1] = 0.
            beta[N-2] = 2.*(N-1)*self.a_k[N-1]
            for k in range(N-2, 0, -1):
                beta[k-1] = 2.*k*self.a_k[k] + beta[k+1]
            beta[0] *= 0.5

            self.dfdx = beta

        # evaluate on supplied grid
        dfdx = C*clenshaw(y, self.dfdx, func='Tn')

        # not too sure I know why I need to reverse the array, (already done above)
        # or why the -1 is out front...
        dfdx *= -1.0

        if (return_instance):
            return Chebyshev(dfdx, a=self.a, b=self.b, zeros=self.zeros,
                             endpoints=self.endpoints)
        else:
            return dfdx

    def integ(self):
        """
        definite integral of Chebyshev series over whole domain
           w_k = int T_k(x) = ((1 + (-1)**k) / (1 - k**2)
                            = 2 / (1 - k**2) for even k
                            = 0              for odd k
        int f(x) dx = sum a_k int T_k(x) dx = sum a_k * w_k
        independent of node choice (zeros vs. extrema)
        """
        if (self.def_integral is not None):
            # save some work if it was already calculated
            return self.def_integral

        # use indefinite integral to calculate definite integral
        self.def_integral = self.antideriv(self.grid.b) - self.antideriv(self.grid.a)

        return self.def_integral

    def antideriv(self, x, return_instance=False):
        """
        indefinite integral of Chebyshev series
           f(x) = sum a_k * T_k(x)
           F(x) = int f(x) dx = sum a_k * int T_k(x) dx = sum b_k * T_k(x)
           relate b_k to a_k using recurrsion relations

           int T_n dx = 0.5*(T_{n+1}/(n+1) - T_{n-1}/(n-1))

           f(x) = a_0 + a_1*x + sum_{k=2}^M a_k * T_k(x)

           F(x) = Const + a_0*x + 0.5*a_1*x**2 + sum_2^M a_k * int T_k dx
                = Const + 0.25*a_1*T0 + a_0*T1 + 0.25*a_1*T_2 + sum_2^M ...
                = ...
                = Const + 0.25*a_1*T0 + (a_0-0.5*a_2)*T1
                        + sum_2^{M-1} [a_{m-1} - a_{m+1}]*T_m/(2*m)
                        + a_{M-1}*TM/(2*M)
                = Const + sum_0^M b_k * T_k(x)

                b_0 = 0.25*a_1
                b_1 = a_0 - 0.5*a_2
                b_k = (a_{k-1} - a_{k+1}) / (2*k)    2 <= k <= M-1
                b_M = a_{M-1}/(2*M)

           Const is chosen s.t. F(-1) = 0
                0 = Const + sum_0^M b_k * T_k(-1)
                Const = - sum_0^M b_k * (-1)**k

           Below, N=len(a_k) so M=N-1

        independent of node choice (zeros vs. extrema)
        """
        x = self._reverse_input(x) # reverse incoming x
        y = self._from_ab(x)        # convert x to [-1,1]
        coeff = self.integ_coeff    # for converting back into [a,b] space

        if (self.Fx is None):
            c = np.zeros_like(self.a_k)
            N = len(c)
            c[0] = 0.25*self.a_k[1]              # first coeff
            c[1] = self.a_k[0] - 0.5*self.a_k[2] # second coeff
            for k in range(2, N-1):              # reccursion, k in [2,N-2] (or k in [2,M-1])
                denom = 2.*k
                c[k] = (self.a_k[k-1] - self.a_k[k+1])/denom
            c[N-1] = self.a_k[N-2]/(2.*(N-1))    # last coeff, k=N-1 (or k=M)

            # trying to set F(-1) = 0, but the array is reversed (see note
            # later about reversing array and the wrong sign). this essentially
            # says F(1) = 0, which must set F(-1) = 0 in a really messed up way...
            if (not self.endpoints):
                # evaluate the coefficients at x=+1 = sum(c)
                Const = -np.sum(c)
                c[0] += Const # lump the integration constant into the T_0 coefficient
            else:
                # evaluate the coefficients at x=xlo=first cheb grid point, because
                # the grid was scaled to include the endpoits
                Const = -clenshaw(self.grid.xhi, c, func='Tn')
                c[0] += Const # lump the integration constant into the T_0 coefficient

            self.Fx = c

        # evaluate result on supplied grid
        Fx = coeff*clenshaw(y, self.Fx, func='Tn')

        # not too sure I know why I need to reverse the array, (already done above)
        # or why the -1 is out front...
        Fx *= -1.0

        if (return_instance):
            return Chebyshev(Fx, a=self.a, b=self.b, zeros=self.zeros,
                             endpoints=self.endpoints)
        else:
            return Fx

    def __call__(self, x):
        """
        evaluate f(x) = sum_0^n a_k * T_k(x) at the given x points
        """
        x = self._reverse_input(x) # reverse incoming x
        y = self._from_ab(x)        # convert x into [-1,1]
        return clenshaw(y, self.a_k, func='Tn')

    def _reverse_input(self, x):
        """
        reverse incoming x
        """
        if (type(x) is np.ndarray):
            if (len(x) > 1):
                x = x[::-1]
            else:
                x = self.b + self.a - x[0]
        else:
            x = self.b + self.a - x
        return x

def antiderivative(fx, x, domain, zeros=True, endpoints=False):
    """
    indefinite integral of data on a chebyshev grid evaluated at given x pts
        F(x) = int_a^x f(t) dt
    constant chosen s.t. F(a) = 0
    """
    cheb = Chebyshev(fx, a=domain[0], b=domain[1], zeros=zeros, endpoints=endpoints)
    Fx = cheb.antideriv(x)
    return Fx

def integral(fx, domain, zeros=True, endpoints=False):
    """
    definite integral of data on a chebyshev grid, returns single number
        I = int_a^b f(x) dx
    """
    cheb = Chebyshev(fx, a=domain[0], b=domain[1], zeros=zeros, endpoints=endpoints)
    I = cheb.integ()
    return I

def derivative(fx, x, domain, zeros=True, endpoints=False):
    """
    1st deriv of data on a chebyshev grid evaluated at given x points
    """
    cheb = Chebyshev(fx, a=domain[0], b=domain[1], zeros=zeros, endpoints=endpoints)
    dfdx = cheb.deriv(x)
    return dfdx

def derivative_matrix(fx, x, domain, zeros=True, endpoints=False):
    """
    1st deriv of data on a chebyshev grid using the differentiation matrices
    """
    N = len(x)

    grid = grids.ChebGrid(len(x), a=domain[0], b=domain[1], zeros=zeros, endpoints=endpoints)
    coeff = grid.deriv_coeff
    x = grid._from_ab(x)

    if (not zeros):
        # D is defined on the [-1,1] grid
        D = np.zeros((N,N)); c = np.ones((N))
        c[0] = c[-1] = 2.
        for i in range(N):
            xi = x[i]
            for j in range(N):
                if (((i == j) and (j == 0)) or  # deal with i=j=0 & i=j=last later
                    ((i == j) and (j == N-1))):
                    continue
                xj = x[j]
                if (i == j):
                    D_ij = -0.5*xj/(1.-xj*xj)
                else:
                    D_ij = (-1.)**(i+j)*c[i]/c[j]/(xi-xj)
                D[i,j] = D_ij
        D[  0,  0] = -(1.+2*(N-1)*(N-1))/6.
        D[N-1,N-1] =  (1.+2*(N-1)*(N-1))/6.
    else:
        # D is defined on the [-1,1] grid
        D = np.zeros((N,N))
        for i in range(N):
            xi = x[i]
            for j in range(N):
                xj = x[j]
                if (i == j):
                    D_ij = 0.5*xj/(1.-xj*xj)
                else:
                    D_ij = (-1.)**(i+j)*np.sqrt((1.-xj*xj)/(1.-xi*xi))/(xi-xj)
                D[i,j] = D_ij

    # convert D to [a,b] grid instead of [-1,1]
    D *= coeff

    # calculate derivative
    dfdx = np.dot(D, fx)

    return dfdx

def cheb_coeffs(fx, zeros=True, dct_type=None):
    """
    return Chebyshev coefficients a_k
       f(x) = sum a_k * T_k(x)
    """
    if (dct_type is None):
        dct_type = 2

    N = len(fx)
    if (zeros):
        coeffs = fft.DCT(fx, type=dct_type, norm=None, window='none')
        coeffs /= N      # adjust the normalization
        coeffs[0] *= 0.5 # first coefficient is special
    else:
        k = N - 1 - np.arange(N-1)
        A = np.hstack((fx[0:N-1], fx[k]))
        F = fft.iFFTc(A, n=None)
        coeffs = np.hstack((F[0], 2*F[1:N-1], F[N-1]))
        coeffs = np.real(coeffs)

    return coeffs

def get_limits(x, zeros=True, endpoints=False):
    """
    return the values of [a,b] given the chebyshev grid
    x is assumed to increase, i.e., x[0] < x[1] < x[2]
    x is assumed in [a,b]
    """
    if (zeros):
        if (endpoints):
            # endpoints are included in the grid
            lo = x[0]; hi = x[-1]
            return lo, hi
        N = len(x)
        x1 = x[0]; xN = x[-1] # these should be close to a & b
        grid = grids.ChebGrid(N, zeros=zeros, endpoints=endpoints)
        y = grid.x
        y1 = y[0]; yN = y[-1] # these should be close to -1 & 1
        # solve the linear mapping [-1,1] --> [a,b] for a & b
        #    y1 = ( 2*x1 - (b+a) ) / (b-a)
        #    yN = ( 2*xN - (b+a) ) / (b-a)
        #
        #    / 1+y1   1-y1 \ /b\ = /2*x1\
        #    \ 1+yN   1-yN / \a/ = \2*xN/
        det = (1+y1)*(1-yN) - (1-y1)*(1+yN)

        num_b = 2*x1*(1-yN) - 2*xN*(1-y1)
        num_a = 2*xN*(1+y1) - 2*x1*(1+yN)

        lo = num_a / det; hi = num_b / det

    else:
        # extrema grid explicitly includes endpoints
        lo = x[0]; hi = x[-1]

    return lo, hi

def is_grid_chebyshev(grid, domain, tol=5e-14):
    """
    determine if grid is a chebyshev grid. grid is assumed in [a,b]
    """

    # grid is assumed to be monotonically increasing
    if (grid[1] < grid[0]):
        grid = grid[::-1]

    N = len(grid); a = domain[0]; b = domain[1]

    # get zeros grid
    cheb_grid = grids.ChebGrid(N, a=a, b=b, zeros=True, endpoints=False)
    y_zero = cheb_grid.x

    # get extrema grid
    cheb_grid = grids.ChebGrid(N, a=a, b=b, zeros=False)
    y_extrema = cheb_grid.x

    # try traditional grids first, i.e., no endpoints in the zeros grid
    x = cheb_grid._from_ab(grid)

    # if difference between incoming grid and the chebyshev
    # grid is larger than the tolerance, then the grid is not chebyshev
    if (np.any(np.greater(np.abs(x-y_zero), tol))):
        is_cheby_zero = False
    else:
        is_cheby_zero = True
    if (np.any(np.greater(np.abs(x-y_extrema), tol))):
        is_cheby_extrema = False
    else:
        is_cheby_extrema = True

    # now try non-standard: zeros grid scaled to include endpoints
    cheb_grid = grids.ChebGrid(N, a=a, b=b, zeros=True, endpoints=True)
    y_zero_end = cheb_grid.x
    x_end = cheb_grid._from_ab(grid)

    if (np.any(np.greater(np.abs(x_end-y_zero_end), tol))):
        is_cheby_zero_end = False
    else:
        is_cheby_zero_end = True

    err_zero     = np.amax(np.abs(x-y_zero))         # standard chebyshev zeros
    err_zero_end = np.amax(np.abs(x_end-y_zero_end)) # cheb zeros scaled to include endpoints
    err_extrema  = np.amax(np.abs(x-y_extrema))      # standard cheyshev extrema

    # a bunch of logic...
    if (is_cheby_zero or is_cheby_extrema or is_cheby_zero_end):
        is_cheby = True
        if (not is_cheby_extrema): # clearly it is a zeros type grid
            cheby_type = 'zeros'
            if (is_cheby_zero and (not is_cheby_zero_end)):   # cleary one or the other
                endpoints = False
            elif (is_cheby_zero_end and (not is_cheby_zero)): # cleary one or the other
                endpoints = True

        else:
            cheby_type = 'extrema'
            endpoints = None

        if (is_cheby_zero and is_cheby_zero_end):
            if ((np.abs(grid[0] - a) < tol) and (np.abs(grid[-1] - b) < tol)):
                endpoints = True
            else:
                endpoints = False

        if (is_cheby_zero and is_cheby_zero_end and is_cheby_extrema):
            print("\n---WARNING: could not determine the chebyshev grid type")
            print("\terror in chebyshev zeros, no  endpoints = {}".format(err_zero))
            print("\terror in chebyshev zeros, yes endpoints = {}".format(err_zero_end))
            print("\terror in chebyshev extrema = {}".format(err_extrema))
            endpoints = None
    else:
        is_cheby = False
        cheby_type = 'not chebyshev'
        endpoints = None

    results = {'chebyshev':is_cheby, 'type':cheby_type, 'endpoints':endpoints,
               'zero-error':err_zero, 'extrema-error':err_extrema,
               'zero-endpoints-error':err_zero_end}
    return results

