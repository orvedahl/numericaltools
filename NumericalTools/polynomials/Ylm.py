"""
Module for performing spherical harmonic calculations

Usage:
    spherical_harmonics.py [options]

Options:
    --lmax=<l>     Max spherical harmonic [default: 63]
"""

from __future__ import print_function
from integrate import integrate
import numpy as np
from scipy.special import sph_harm
from numpy import sin,cos
import sys

def factorial_ratio(ell):
    """
    compute np.sqrt( (2l)! / 4^l / l! / l! )
    """
    l_values = np.arange(1, ell+1, 1.) # [1,ell], inclusive
    ratio = 1.0
    for l in l_values:
        ratio *= np.sqrt( (l - 0.5)/ l )
    return ratio

def Ylm_sph_harm(l, m, polar_angle, azimuthal_angle):
    """
    calculate Y_lm(theta,phi) = A_lm*P_lm(cos(theta))*exp(1j*m*phi)
    where theta=polar & phi=azimuth
    """
    Ylm = sph_harm(l,m,azimuthal_angle,polar_angle)
    return Ylm

def Y_l_minus_m(Ylm, m):
    """
    input Ylm array (or value)
    return Y_l^(-m) = (-1)^m * (Y_l^m)^*
    """
    m = abs(m)
    Y_l_neg_m = (-1.0)**m*np.conj(Ylm)
    return Y_l_neg_m

def Y_lm(lmax, polar, azimuth):
    """
    lmax    = integer maximum ell value
    polar   = array of polar angles
    azimuth = array of azimuthal angles

    only positive m is returned
    nell = nm = lmax + 1
    return array of Y(m,l,theta,phi)
    """
    nth  = len(polar)
    nphi = len(azimuth)
    nell = lmax + 1
    nm   = nell

    Ylm = np.zeros((nm, nell, nth, nphi), dtype=np.complex128)

    for m in range(nm):

        # l=m parts
        l=m
        amp = factorial_ratio(m)*np.sqrt((m+0.5)/2./np.pi)
        if(m%2 !=0):
           amp *= -1.0
           power = m//2 + 0.5  # the '//' is integer division
        else:
           power = m//2
        for t in range(nth):
            th = polar[t]
            tmp = np.sin(th) # = 1-x**2 = 1.0 - np.cos(th)*np.cos(th)
            Ylm[m,l,t,:] = amp*tmp**power

            # l=m+1 parts
            if (m < lmax):
                Ylm[m,l+1,t,:] = Ylm[m,l,t,:]*np.cos(t)*np.sqrt(2.0*l+3.)

        # general, l > m+1
        for l in range(m+2, lmax+1): # l in [m+2,lmax] inclusive
            for t in range(nth):
                th = polar[t]
                x = np.cos(th)
                amp = (l-1)**2 - m*m
                amp /= 4.0*(l-1)**2 - 1.0
                amp = np.sqrt(amp)
                tmp = Ylm[m,l-1,t,:]*x - amp*Ylm[m,l-2,t,:]
                amp = np.sqrt((4.0*l*l-1.0)/(l*l-m*m))
                Ylm[m,l,t,:] = tmp*amp

        # now add the complex bits
        for p in range(nphi): 
            phi = azimuth[p]
            Ylm[m,:,:,p] *= np.exp(1j*m*phi)

    return Ylm

def sph_transform(f, theta, phi, lmax=None):
    # calculate forward spherical harmonic transform:
    #    f(l,m,...) = int_0^pi dphi int_0^2pi (Y_1^m)* sin(theta) f(theta,phi,...) dtheta
    #
    # (A)* = np.conj(A)
    #
    # incoming f has assumed order [theta, phi, ...], where "..." need not be specified

    f_shape = np.shape(f); ntheta = f_shape[0]; nphi = f_shape[1]
    C_shape = list(f_shape)
    if (lmax is None):
        lmax = int(2.*ntheta/3. - 1.) # assumes 2/3 dealias

    # get the various spherical harmonics
    Ylm_star = np.conj(Y_lm(lmax, theta, phi))
    Yshape = np.shape(Ylm_star)

    # get shape of (nm, nell, ...) where "..." comes from shape of incoming f
    # and the (nm, nell) part is from the shape of Ylm
    Cshape[0] = Yshape[0]; Cshape[1] = Yshape[1]
    Cshape = tuple(Cshape)
    Ishape = (Yshape[0], Yshape[1]) + f_shape

    C_lm      = np.zeros(Cshape, dtype=np.complex128)
    integrand = np.zeros(Ishape, dtype=np.complex128)

    # multiply by sin(theta)
    itheta = range(ntheta); iphi = range(nphi)
    for t in itheta:
        sin_t = sin(theta[t])
        for p in iphi:
            #         nm,nell,nth,nphi,...  =   nth,nphi,...             nm, nell,nth,nphi
            integrand[:, :,   t,  p,   ...] = f[t,  p,   ...]*sin_t*Ylm_star[:,:,t,p]

    # integrate over theta & phi
    C_lm = integrate.integrate_2d(integrand, theta, phi, axes=[2,2])

    # size should be [m,l,...]
    return C_lm


if __name__ == "__main__":
    from docopt import docopt
    args = docopt(__doc__)

    lmax = int(args['--lmax'])
    N    = int(3.*(lmax + 1)/2.)

    dtheta = np.pi/N; dphi = 2*np.pi/N
    theta = np.arange(0,   np.pi, dtheta)
    phi   = np.arange(0, 2*np.pi, dphi)

    Ylm = Y_lm(lmax, theta, phi)

    # pick 5 pairs of angles
    l=2; m=1
    print("i, theta, phi, Y_direct, Y_me")
    for k in [0, 0.2*N, 0.4*N, 0.6*N, 0.8*N, N-1]:
        i = int(k)
        t = theta[i]
        p = phi[i]
        Y_direct = -np.sqrt(15./8./np.pi)*np.sin(t)*np.cos(t)*np.exp(1j*p)
        Y_me = Ylm[m,l,i,i]
        print("{:2d}, {:.4f}, {:.4f}, {}, {}".format(i,t,p,Y_direct,4*Y_me))

    import matplotlib.pyplot as plt
    plt.clf()
    Y_direct = -np.sqrt(15./8./np.pi)*np.sin(theta)*np.cos(theta)*np.exp(1j*phi[0])
    plt.plot(theta, np.real(Y_direct), label='direct')
    plt.plot(theta, np.real(Ylm[1,2,:,0]), label='me')
    plt.legend()
    plt.show()

    plt.clf()
    Y_direct = np.sqrt(3/4./np.pi)*np.cos(theta)
    plt.plot(theta, np.real(Y_direct), label='direct')
    plt.plot(theta, np.real(Ylm[0,1,:,0]), label='me')
    plt.legend()
    plt.show()


