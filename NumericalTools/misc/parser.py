"""
Interface to handle various parsing routines
"""

# first try to import docopt, assuming it is installed.
# if that fails, import the direct source code for docopt
try:
    from docopt import docopt as _parser
except ImportError:
    from _docopt import docopt as _parser

def parseArgs(docstring, **kwargs):
    """
    Wrapper to docopt for parsing command line arguments.

    docstring --- the __doc__ string of the file
    kwargs --- keyword arguments that get passed to docopt
    """
    args = _parser(docstring, **kwargs)
    return args

