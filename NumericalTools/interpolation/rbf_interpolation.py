"""
Perform interpolation using Radial Basis Functions

Usage:
    rbf_interpolation.py [options]

Options:
    --debug      Debugging flag [default: False]
    --N=<n>      Test using <n> data points [default: 64]
    --dim=<d>    Test using <d> dimensional data [default: 1]
    --eps=<e>    Shape parameter [default: -1]
    --func=<f>   What RBF to use [default: thin-plate]
"""

from __future__ import print_function, division

import sys
import numpy as np
#from scipy.special import xlogy
from scipy import linalg
from six import callable, get_method_function, get_function_code

class Rbf(object):
    """
    Rbf(*args)

    A class for radial basis function approximation/interpolation to
    N-dimensional scattered data.

    Parameters
    ----------
    *args : arrays
        x, y, z, ..., d, where x, y, z, ... are coordinates of the nodes
        and d is the array of values at the nodes
    function : str or callable, optional
        The radial basis function, based on the radius, r, given by the norm
        (default is Euclidean distance; the default is 'multiquadric'::

        'multiquadric': sqrt((r/self.epsilon)**2 + 1)
        'inverse': 1.0/sqrt((r/self.epsilon)**2 + 1)
        'gaussian': exp(-(r/self.epsilon)**2)
        'linear': r
        'cubic': r**3
        'quintic': r**5
        'thin-plate': r**2*log(r)

        If callable, then it must take 2 arguments (self, r). The epsilon
        parameter will be available as self.epsilon. Other keyword arguments
        passed in will be available as well.

    epsilon : float, optional
        Adjustable constant for gaussian or multiquadrics functions
        - defaults to approximate average distance between nodes (which is
        a good start).
    smooth : float, optional
        Values greater than zero increase the smoothness of the approximation.
        0 is for interpolation (default), the function will always go through
        nodal points in this case.
    norm : callable, optional
        A function that returns the 'distance' between two points, with inputs
        as arrays of positions (x, y, z, ...), and an output as an array of
        distance. E.g., the default::

            def euclidean(x1, x2):
                return sqrt( ((x1 - x2)**2).sum(axis=0) )

        which is called with x1=x1[ndims,newaxis,:] and x2=x2[ndims,:,newaxis]
        such that the result is a matrix of the distances from each point in x1
        to each point in x2.

    Examples
    --------
    >>> import Rbf_interpolation
    >>> x,y,z,d = np.random.rand(4, 50)
    >>> rbfi = Rbf(x, y, z, d) # RBF interpolator instance
    >>> xi = yi = zi = np.linsapce(0, 1, 20)
    >>> di = rbfi(xi, yi, zi)  # interpolated values
    >>> di.shape
    (20,)

    """

    def _euclidean_norm(self, x1, x2):
        return np.sqrt(((x1 - x2)**2).sum(axis=0))

    def _h_gaussian(self, r):
        return np.exp(-(r/self.epsilon)**2)

    def _h_multiquadric(self, r):
        return np.sqrt((r/self.epsilon)**2 + 1)

    def _h_inverse_multiquadric(self, r):
        return 1.0/np.sqrt((r/self.epsilon)**2 + 1)

    def _h_linear(self, r):
        return r

    def _h_cubic(self, r):
        return r**3

    def _h_quintic(self, r):
        return r**5

    def _h_thin_plate(self, r):
        # when r=0, return 0
        phi = np.zeros_like(r)
        ind = np.where(r!=0.)
        phi[ind] = r[ind]**2*np.log(r[ind])
        return phi

    def _init_function(self, r):
        if (isinstance(self.function, str)):
            # function was a string, map it to the desired RBF
            self.function = self.function.lower()
            _mapped = {'inverse':'inverse_multiquadric',
                       'inverse multiquadric':'inverse_multiquadric',
                       'thin-plate':'thin_plate'}
            if (self.function in _mapped):
                self.function = _mapped[self.function]

            func_name = "_h_" + self.function
            if (hasattr(self, func_name)):
                self._function = getattr(self, func_name)
            else:
                functionlist = [x[3:] for x in dir(self) if x.startswith('_h_')]
                _passed = "Unrecognized function = {}\n".format(self.function)
                raise ValueError(_passed + "Function must be callable or one of " +
                                 ", ".join(functionlist))
        elif (callable(self.function)):
              # user supplied their own function, map it to _function and
              # make sure it has the proper number of arguments
              allow_one = False
              if ((hasattr(self.function, 'func_code')) or \
                         (hasattr(self.function, '__code__'))):
                  val = self.function
                  allow_one = True
              elif (hasattr(self.funciton, 'im_func')):
                  val = get_method_function(self.function)
              elif (hasattr(self.function, '__call__')):
                  val = get_method_function(self.function.__call__)
              else:
                  raise ValueError("Cannot determine number of arguments to function")

              argcount = get_function_code(val).co_argcount
              if (allow_one and argcount == 1):
                  self._function = self.function
              elif (argcount == 2):
                  if (sys.version_info[0] >= 3):
                      self._function = self.function.__get__(self, Rbf)
                  else:
                      import new
                      self._function = new.instancemethod(self.function, self, Rbf)
              else:
                  raise ValueError("Function argument must take 1 or 2 arguments")

        else:
            functionlist = [x[3:] for x in dir(self) if x.startswith('_h_')]
            raise ValueError("Function must be callable or one of " +
                                 ", ".join(functionlist))
        a0 = self._function(r)
        if (a0.shape != r.shape):
            raise ValueError("Callable must take array and return array of same shape")
        return a0

    def __init__(self, *args, **kwargs):
        # extract individual coordinates and function values
        self.xi = np.asarray([np.asarray(a, dtype=np.float64).flatten()
                             for a in args[:-1]])
        self.N = self.xi.shape[-1]
        self.di = np.asarray(args[-1]).flatten()

        if (not all([x.size == self.di.size for x in self.xi])):
            raise ValueError("All arrays must be equal length")

        # extract the various keyword arguments
        self.norm = kwargs.pop("norm", self._euclidean_norm)
        r = self._call_norm(self.xi, self.xi) # get matrix of distances
        self.epsilon = kwargs.pop("epsilon", None)
        if (self.epsilon is None):
            # default is average distance between nodes based on bounding hypercube
            dim = self.xi.shape[0]
            ximax = np.amax(self.xi, axis=1)
            ximin = np.amin(self.xi, axis=1)
            edges = ximax-ximin
            edges = edges[np.nonzero(edges)]
            self.epsilon = np.power(np.prod(edges)/self.N, 1.0/edges.size)
        self.smooth = kwargs.pop("smooth", 0.0)

        self.function = kwargs.pop("function", "multiquadric")

        # save every other option that was passed
        for item, value in kwargs.items():
            setattr(self, item, value)

        # use RBF function and evaluate at the matrix of distances
        # to get the `A' matrix. use this and the given function values
        # to solve for the weights
        self.A = self._init_function(r) - np.eye(self.N)*self.smooth
        self.nodes = linalg.solve(self.A, self.di)

    def _call_norm(self, x1, x2):
        # make arrays multi dimensional
        # such that x1-x2 is an array
        if (len(x1.shape) == 1):
            x1 = x1[np.newaxis, :]
        if (len(x2.shape) == 1):
            x2 = x2[np.newaxis, :]
        x1 = x1[..., :, np.newaxis]
        x2 = x2[..., np.newaxis, :]
        return self.norm(x1, x2)

    def __call__(self, *args):
        # evaluate the interpolant at the given points
        args = [np.asarray(x) for x in args]
        if (not all([x.shape == y.shape for x in args for y in args])):
            raise ValueError("Array lengths must be equal")
        shp = args[0].shape
        xa = np.asarray([a.flatten() for a in args], dtype=np.float64)
        r = self._call_norm(xa, self.xi)
        return np.dot(self._function(r), self.nodes).reshape(shp)

