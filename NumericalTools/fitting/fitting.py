"""
Module for performing least-square fits to data

Usage:
    fitting.py [options]

Options:
    --N=<n>        Number of grid points [default: 128]
    --method=<m>   How to get errors, mc, none, jack, boot [default: none]
    --no-plots     Suppress the plots [default: False]
    --func=<f>     Set what function to do: 0, 1 [default: 0]
    --debug        Debug mode [default: False]
"""

from __future__ import print_function
from scipy.optimize import leastsq
import scipy.stats as stats
import numpy as np
import sys

def same_value(x1, x2, tol=None):
    """
    determine if two numbers are statistically different or not

    x1 = (a, da) --- tuple containing central value and error
    x2 = (b, ba) --- tuple containing central value and error
    tol          --- tolerance
    """

    a = x1[0]; da = x1[1]
    b = x2[0]; db = x2[1]

    diff = a - b
    err  = np.sqrt(da**2 + db**2)

    if (abs(diff) <= err):
        same = True
    else:
        same = False

    return same, [diff, err]

def general_fit(x, y, fit_func, init_guess, error_method='none',
                full_output=False, confidence=0.68, Ntrials=None, seed=56,
                upper_lower_errors=False, func_args=None, debug=False, mcsigma=0.1,
                **kwargs):

    np.random.seed(seed)
    if (func_args is None):
        func_args = ()
    method = error_method.lower()
    info = {}

    if (Ntrials is None):
        Ntrials = int(10.0*len(y))

    # get initial guess for fit parameters and take this as "true"
    _par0, _err, _chi2, _chir, _pte = fit_data(x, y, fit_func, init_guess,
                                               func_args=func_args, **kwargs)
    if (_par0 is None):
        print("\n\tERROR: initial fit failed\n")
        sys.exit()
    yTrue = fit_func(x, _par0, *func_args)

    # keep track of what method was requested. also serves as list
    # of supported methods
    chosen_method = {'none':False, 'mc':False, 'bootstrap':False,
                     'jackknife':False, 'analytic-line':False}

    if (method not in chosen_method.keys()):
        print("\nError: unsupported error estimation choice: {}\n".format(method))
        print("\tpossible choices include:")
        for key in chosen_method.keys():
            print("\t\t{}".format(key))
        sys.exit()

    chosen_method[method] = True

    if (chosen_method['none']):
        # use intrinsic scipy.optimize.leastsq (MINPACK's lmdif)
        # to find parameters and parameter errors
        # the errors are found by estimating the Hessian matrix, H
        # where H = d^2 chi**2 / da_i da_j = matrix of second derivs
        #      if (dy is None):
        #          --an arbitrary dy is chosen
        #          --the fit is performed
        #          --variance is estimated as sigma**2 ~= reduced_chi_squared = constant
        #          --Hessian is calculated using constant sigma**2 estimate
        #          --H is inverted
        #          --diagonals give the parameter variances
        #          --calculated reduced chi**2 no longer meaningful, since it was used
        #            to estimate the errors. relying on it now would be pretty damn circular
        #      else:
        #          --Hessian matrix actually has meaning already, so just calculate it
        #          --H is inverted
        #          --diagonals give the parameter variances
        #          --calculated reduced chi**2 actually meaningful
        _param = _par0
        _error = _err
        info['chi_squared']  = _chi2
        info['reduced_chi2'] = _chir
        info['pte'] = _pte

        calculate_errors = False
        do_trials = False

    elif (chosen_method['analytic-line']):
        # use intrinsic scipy.optimize.leastsq (MINPACK's lmdif)
        # to find parameters and parameter errors
        _param = _par0
        _error = _err
        info['chi_squared']  = _chi2
        info['reduced_chi2'] = _chir
        info['pte'] = _pte

        calculate_errors = True
        do_trials = False

    elif (chosen_method['mc']):
        # use Monte-Carlo simulations to estimate parameter errors

        def compute_trial(i):
            # use random x values drawn uniformally from x-domain
            xTrial = np.zeros_like(x)
            for i in range(np.shape(x)[0]):
                maxx = np.max(x[i]); minx = np.min(x[i])
                xTrial[i] = np.sort(np.random.uniform(low=minx,
                                         high=maxx, size=np.size(x[i])))

            # get y values using initial fit parameters and new x values
            ystart = fit_func(xTrial, _par0, *func_args)

            # add noise to y values using a healthy width
            ypert = np.random.normal(scale=mcsigma, size=np.size(ystart))

            yTrial = ystart + ypert

            return xTrial, yTrial

        calculate_errors = True
        do_trials = True

    elif (chosen_method['bootstrap']):
        # use bootstrap method to estimate parameter errors

        N = len(y)
        indices = np.arange(N)
        def compute_trial(i, **kwargs):
            # extract random data set from input (x,y) data with replacement
            ind = np.random.choice(indices, size=N, replace=True)

            # take transposes so xTrial[0] still works when shape
            # of x is (k,N) for k independent variables
            xTrial = (x.T[ind]).T
            yTrial = (y.T[ind]).T
            return xTrial, yTrial

        calculate_errors = True
        do_trials = True

    elif (chosen_method['jackknife']):
        # use jackknife method to estimate parameter errors

        N = len(y)
        Ntrials = N
        def compute_trial(i, **kwargs):
            # take transposes so xTrial[0] still works when shape
            # of x is (k,N) for k independent variables
            xt = list(x.T); yt = list(y.T)
            del xt[i]
            del yt[i]
            return (np.array(xt)).T, (np.array(yt)).T

        calculate_errors = True
        do_trials = True

    else:
        print("\n\tERROR: how did you even get here? method = {}\n".format(method))
        sys.exit()

    if (do_trials):
        _params = np.array([])
        for iTrial in range(Ntrials):

            xTrial, yTrial = compute_trial(iTrial)

            # find fit parameters
            _par,_err,_chi2,_chir,_pte = fit_data(xTrial, yTrial, fit_func,
                                            init_guess, func_args=func_args, **kwargs)

            if (_par is None): # fit failed, do next trial
                continue

            # store the fit parameters if the fit was successful
            if (np.size(_params) >= 1):
                _params = np.vstack((_params, _par))
            else:
                _params = np.copy(_par)

        Nsuccess = np.shape(_params)[0]
        print("Completed {:} successful trials ({:.1f} %)".format(Nsuccess,
                                                     100.0*Nsuccess/Ntrials))

    if (calculate_errors):
        def cntr_confidence_interval(data, confidence_interval, method=1, cntr=None):
            # both methods seem to work, but I understand method==1

            if (method == 0):
                # given the sample mean, sample variance and true mean,
                # the quantity T=(xbar - mu)/(S/sqrt(N)) will be student-t distributed
                # with N-1 d.o.f. where xbar=1/N*sum(x) and S**2 = sum[(x-xbar)**2]/(N-1)
                # therefore:
                # Pr(-t < T < t) = C --> Pr(xbar - t*S/sqrt(N) < mu < xbar + t*S/sqrt(N)) = C
                # i.e., the probability for T to be within (-t,t) is C
                # now solve for mu, the true mean:
                # mu = xbar +/- t*S/sqrt(N) for a C confidence interval e.g., C=0.9
                cntr = np.mean(data); S = np.std(data, ddof=1)
                N = len(data); alpha = 0.5 + 0.5*confidence_interval
                t = stats.t.ppf(alpha, N-1)
                lower = cntr - t*S/sqrt(N)
                upper = cntr + t*S/sqrt(N)

            elif (method == 1):
                # this brute force finds the upper/lower intervals
                # --find central value
                # --split sorted data into above-cntr and below-cntr
                # --find the C-th value of the above-cntr ---> this is the upper
                # --find the (1-C)-th value in the below-cntr ---> this is the lower
                if (cntr is None):
                    cntr = np.median(data)
                ghi = np.where(data >= cntr)[0]
                glo = np.where(data <  cntr)[0]
                sortlo = np.sort(data[glo])
                sorthi = np.sort(data[ghi])
                lower = sortlo[np.int((1.-confidence_interval)*np.size(sortlo))]
                upper = sorthi[np.int(    confidence_interval *np.size(sorthi))]

            return cntr, lower, upper

        # figure out the error in the trials
        if (chosen_method['jackknife']):
            pseudo_par = np.zeros_like(_params)
            for j in range(Nsuccess):
                # generate a pseudo stat for each trial
                pseudo_par[j,:] = Nsuccess*_par0 - (Nsuccess-1)*_params[j,:]

            # get best guess of each parameter, by averaging over trials
            par_est = np.mean(pseudo_par, axis=0)

            # get variance of pseudo values: var1 = sum (x_i - mu)**2 / (N-1)
            # get standard error of parameter estimate as var = var1 / N
            # so final result is var = sum(x_i - mu)**2 / N / (N-1)
            var_est = np.zeros_like(par_est)
            for i in range(len(par_est)):
                estimate = 0.0
                for j in range(Nsuccess):
                    estimate += (pseudo_par[j,i] - par_est[i])**2
                var_est[i] = estimate/(1.*Nsuccess*(1.*Nsuccess-1.))

            _param = par_est
            _stdev = np.sqrt(var_est)
            _error = []

            # now calculate the confidence intervals
            for i in range(len(_param)):
                #dof = Nsuccess-1
                #t = stats.t.ppf(confidence, dof)
                #alpha = 1. - 0.5*(1. - confidence)
                #lower, upper = stats.t.interval(alpha=alpha, df=dof,
                #                                loc=_param[i], scale=_stdev[i])
                #cntr = _param[i]
                cntr, lower, upper = cntr_confidence_interval(pseudo_par[:,i], confidence,
                                                              cntr=_param[i])
                lerror, uerror = cntr-lower, upper-cntr
                if (upper_lower_errors):
                    _error.append([lerror, uerror])
                else:
                    _error.append(0.5*(lerror+uerror))

                if (debug):
                    print("\tparameter {}".format(i))
                    print("\t\tcntr = {}".format(cntr))
                    print("\t\tmin = {}, max = {}".format(np.min(pseudo_par[:,i]),
                                                          np.max(pseudo_par[:,i])))
                    print("\t\tlower = {}, upper = {}".format(lower, upper))
                    print("\t\tlerror = {}, uerror = {}".format(lerror, uerror))
                    p = ((pseudo_par[:,i] >= lower) & (pseudo_par[:,i] <= upper)).sum()
                    p *= 100.0/len(pseudo_par[:,i])
                    print("\t\t{:.2f} % are in confidence interval".format(p))

        elif (chosen_method['analytic-line']):
            # we are fitting y = A + B*x
            # sec 15.5.1 Calculation of the Gradient & Hessian, Numerical Recipes, pg 800
            # it is also in pg 109 of Bevington & Robinson 'Data Reduction & Error Analysis'
            # 1) get best estimate for each parameter by single fit, done above
            # 2) get estimate of the error:
            #        chi**2 = sum_i [ (y_i - f(x_i,a,b)) / dy_i ]**2
            #          dy_i = error of i-th data point
            #      f(x_i,a) = a + b*x = fitting func
            #
            #      a) calculate Hessian = second deriv of chi**2 w.r.t. a & b
            #      b) multiply by 0.5, just because...
            #      c) invert the matrix
            #      d) diagonals now give the variance in the fit parameters

            _param = _par0; _error = []

            # first parameter is assumed to be constant, A
            # second parameter is assumed to be the linear term, B
            if ('dy' in kwargs.keys()):
                if (kwargs['dy'] is None):
                    dy_not_given = True
                else:
                    dy_not_given = False

            else:
                dy_not_given = True

            # calculate the inverse of the Hessian
            if (dy_not_given):
                # dy not given, proceed assuming dy_i = 1 for all data points
                sigma = 1.0
                if (True):
                    # use reduced chi-squared as estimate for sigma
                    # see pg 780 of Numerical Recipes, end of section 15.1.1
                    sigma = np.sqrt(info['reduced_chi2'])
                Num = len(x)
                sum_x = np.sum(x); sum_x2 = np.sum(x*x)
                det = Num*sum_x2 - (sum_x)**2
                C_aa = sigma**2 * sum_x2 / det
                C_bb = sigma**2 * Num / det
            else:
                # dy was given, so include it
                sum_s = np.sum(1./dy**2); sum_xs = np.sum(x/dy**2)
                sum_x2s = np.sum(x**2/dy**2)
                det = sum_s*sum_x2s - sum_xs**2
                C_aa = sum_x2s / det
                C_bb = sum_s / det

            lerror_a = uerror_a = np.sqrt(C_aa)
            lerror_b = uerror_b = np.sqrt(C_bb)

            if (upper_lower_errors):
                _error.append([lerror_a, uerror_a])
                _error.append([lerror_b, uerror_b])
            else:
                _error.append(0.5*(uerror_a + lerror_a))
                _error.append(0.5*(uerror_b + lerror_b))

        else:
            _param = []; _error = []
            for i in range(len(_par0)):
                cntr, lower, upper = cntr_confidence_interval(_params[:,i], confidence)
                lerror = cntr-lower
                uerror = upper-cntr
                _param.append(cntr)
                if (upper_lower_errors):
                    _error.append([lerror, uerror])
                else:
                    _error.append(0.5*(uerror + lerror))

                if (debug):
                    print("\tparameter {}".format(i))
                    print("\t\tcntr = {}".format(cntr))
                    print("\t\tmin = {}, max = {}".format(np.min(_params[:,i]),
                                                          np.max(_params[:,i])))
                    print("\t\tlower = {}, upper = {}".format(lower, upper))
                    print("\t\tlerror = {}, uerror = {}".format(lerror, uerror))
                    p = ((_params[:,i] >= lower) & (_params[:,i] <= upper)).sum()
                    p *= 100.0/len(_params[:,i])
                    print("\t\t{:.2f} % are in confidence interval".format(p))

        dof = len(y) - len(init_guess)
        _chi2 = np.sum((y-fit_func(x,_param,*func_args))**2) # this assumes dy=None
        _chir = _chi2/dof
        _pte  = 1. - stats.chi2.cdf(_chi2, dof)
        info['chi_squared']  = _chi2
        info['reduced_chi2'] = _chir
        info['pte'] = _pte

    if (full_output):
        return _param, _error, info
    else:
        return _param, _error

def fit_data(x, y, fit_func, init_guess, dy=None, func_args=None, **kwargs):
    """
    method to perform fit of f(x) to the data

    x          --- independant variable
    y          --- dependant variable, f(x)
    fit_func   --- function to be fit to the (x,y) data
                     should have the calling sequence of fit_func(x, params, *args)
    init_guess --- initial guess for the parameters
    dy         --- (optional) error in the y data, defaults to None
    func_args  --- (optional) tuple of other arguments required by fit_func

    Example:
           # define the function you wish to fit to (x,y) data
           def fit_func(x, avec):
               yint, slope = avec
               return sigma*x + yint

           # do fit
           a0 = (1.2, 4.6)
           param, param_err = fit_data(x, y, fit_func, a0)
    """

    if (func_args is None):
        func_args = ()

    if (dy is None):
        # error was not provided
        def residual(avec, x, y, *args):
            return (y - fit_func(x, avec, *args))
        args = (x, y,) + func_args
    else:
        # error was provided
        def residual(avec, x, y, dy, *args):
            return (y - fit_func(x, avec, *args))/dy
        args = (x, y, dy,) + func_args

    success = True

    # call fitting routine
    params, param_err, infodict, errmsg, ierr = \
                           leastsq(residual, init_guess, args=tuple(args), full_output=1)

    if (ierr not in [1,2,3,4]):
        print("\nOptimal parameters not found: {}\n".format(errmsg))
        success = False

    if (success):
        error_success = True

        dof = len(y) - len(init_guess)
        if ((dof > 0) and (param_err is not None)):
            # sum of squares of residual divided by D.O.F.
            # see scipy curve_fit, their cost variable = s_sq * d.o.f.
            if (dy is None):
                s_sq = np.sum(residual(params, x, y, *func_args)**2) / dof
            else:
                s_sq = np.sum(residual(params, x, y, dy, *func_args)**2) / dof
            param_err *= s_sq
            chi_sq = dof*s_sq
            red_chi_sq = s_sq
            PTEvalue = 1. - stats.chi2.cdf(chi_sq, dof) # p value = chi2.cdf(), PTE = 1 - p
            one_stdev_param_err = np.sqrt(np.diag(param_err))
        else:
            print("\nCould not extract error estimates, dof = {}, param_err is None = {}".format(dof, param_err is None))
            error_success = False

    if (success and error_success):
        return (params, one_stdev_param_err, chi_sq, red_chi_sq, PTEvalue)
    else:
        return (None, None, None, None, None)

if __name__ == "__main__":

    import matplotlib.pyplot as plt
    from docopt import docopt
    args = docopt(__doc__)

    # get command line args
    N = int(args['--N'])
    debug = args['--debug']
    func = int(args['--func'])

    # setup the data and define fitting function & residual
    if (func == 0):
        x = np.linspace(-2.5,3.5,N)
        true = (2.3, 4, np.pi, -2)
        a0 = (2., 3.5, 3., -1.5)
        def fit_func(x, avec):
            a,b,c,d = avec
            return a*x**2 + b*np.sin(c*x) + d
        sigma = 0.7
    elif (func == 1):
        x = np.linspace(-1.5,1.5,N)
        true = (2.3, 0.15, 3*np.pi, -2)
        a0 = (2.2, 0.12, 9.5, -1.7)
        def fit_func(x, avec):
            a,b,c,d = avec
            return a*np.exp(b*x**2) + np.sin(c*x) + d
        sigma = 0.1

    # add noise
    np.random.seed(42)
    df = sigma*np.random.randn(len(x))
    fx = fit_func(x, true) + df
    dfplot = df

    if (debug):
        plt.clf()
        plt.errorbar(x, fx, yerr=dfplot, label='data', marker='x', linestyle='', color='r')
        plt.plot(x, fit_func(x, a0), label='init guess', marker='', linestyle='--', color='k')
        plt.plot(x, fit_func(x, true), label='true', marker='', linestyle='-', color='b')
        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.legend(loc='upper left', numpoints=1)
        plt.show()

    # do fit
    if (args['--method'] == 'none'):
        print("\nFitting results:\n")
        error_method = 'none'
    elif (args['--method'] == 'mc'):
        print("\nFitting results (Monte Carlo):\n")
        error_method = 'mc'
    elif (args['--method'] == 'boot'):
        print("\nFitting results (Bootstrap):\n")
        error_method = 'bootstrap'
    elif (args['--method'] == 'jack'):
        print("\nFitting results (Jackknife):\n")
        error_method = 'jackknife'
        df = None

    mcsigma = 0.2
    yfit, yerr, info = general_fit(x, fx, fit_func, a0, error_method=error_method,
                                   full_output=True, dy=df, debug=debug, mcsigma=mcsigma)
    chi2 = info['chi_squared']
    red_chi2 = info['reduced_chi2']
    PTE = info['pte']

    print("\tTrue parameters:")
    print("\t\ta = {}".format(true[0]))
    print("\t\tb = {}".format(true[1]))
    print("\t\tc = {}".format(true[2]))
    print("\t\td = {}".format(true[3]))
    print("\n\tFit parameters:")
    print("\t\ta = {} +/- {} ({:.2f} %)".format(yfit[0], yerr[0], 100*abs(yerr[0]/yfit[0])))
    print("\t\tb = {} +/- {} ({:.2f} %)".format(yfit[1], yerr[1], 100*abs(yerr[1]/yfit[1])))
    print("\t\tc = {} +/- {} ({:.2f} %)".format(yfit[2], yerr[2], 100*abs(yerr[2]/yfit[2])))
    print("\t\td = {} +/- {} ({:.2f} %)".format(yfit[3], yerr[3], 100*abs(yerr[3]/yfit[3])))
    print("\tFit results:")
    print("\t\tChi squared = {}".format(chi2))
    print("\t\tReduced Chi squared = {}".format(red_chi2))
    print("\t\tPTE value = {}".format(PTE))
    print()

    if (not args['--no-plots']):
        plt.clf()
        plt.errorbar(x, fx, yerr=dfplot, label='data', marker='x', linestyle='', color='r')
        plt.plot(x, fit_func(x, yfit), label='fit', marker='', linestyle='-', color='k')
        plt.plot(x, fit_func(x, a0), label='init guess', marker='', linestyle='--', color='k')
        plt.plot(x, fit_func(x, true), label='true', marker='', linestyle='-', color='b')
        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.legend(loc='upper left', numpoints=1)
        plt.show()

