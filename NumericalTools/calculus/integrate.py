"""
Module for performing definite and indefinite integration

Supported options, default values listed in []:
    --data_reversed, is grid monotonically increasing/decreasing [False]
    --method, what method to use ['trap']

    --indefinite, calculate an indefinite integral [False]

    --zeros, use chebyshev zeros grid [True]
    --endpoints, use chebyshev zeros grid scaled to include endpoints [False]

Methods for Non-Uniform grids:
    --cheb: chebyshev zeros & extrema
    --trap: trapezoid (2nd order)
    --simps: simpson (2nd order)
    --8th: only 2nd order on non-uniform grid
    --cheb-end: chebyshev zeros grid scaled to include endpoints

Methods for Uniform grids:
    --trap: trapezoid (2nd order)
    --simps: simpson, fit parabola to data and integrate (4th order)
    --8th: fit quartic to data and integrate (8th order)

"""

from __future__ import print_function
import numpy as np
from scipy import linalg
from numpy import trapz as trapezoid_integrate
from scipy.integrate import cumtrapz as indef_trap
from ..polynomials.chebyshev import integral as chebyshev_integ
from ..polynomials.chebyshev import antiderivative as chebyshev_antideriv
from ..polynomials.chebyshev import get_limits, is_grid_chebyshev
from sys import exit

def volume_avg(data_in, grid_in, **kwargs):
    """
    volume average spherical data:
        f_avg = int(4*pi*f*r**2 dr) / int(4*pi*r**2 dr)
    """
    vol_avg = integrate(data_in*grid_in*grid_in, grid_in, **kwargs)
    volume  = integrate(grid_in*grid_in, grid_in, **kwargs)
    f_avg = vol_avg/volume
    return f_avg

def integrate(data_in, grid_in, method='trap', data_reversed=False, indefinite=False,
              **kwargs):
    """
    method to perform 1D integration
    """
    supported = ['cheb', '8th', 'simps', 'trap', 'cheb-end']

    method = method.lower()
    if (method not in supported):
        print("\n---ERROR: unsupported integration method = {}".format(method))
        print("\t\tsupported methods = {}".format(supported))
        exit()

    if (method in ['8th']):
        print("\n---WARNING: integration not heavily tested for method = {}".format(method))

    if (data_reversed):
        data = data_in[::-1]
        grid = grid_in[::-1]
    else:
        data = data_in
        grid = grid_in

    # choose integrator and calling args based on method
    if (method == 'simps'):
        integrator_args = {}
        integrator = simpson

    elif (method == '8th'):
        integrator_args = {}
        integrator = eighth_order

    elif (method == 'trap'):
        integrator_args = {'indefinite':indefinite}
        integrator = trapezoid

    elif ('cheb' in method):
        endpoints = kwargs.pop("endpoints", False)
        if (method == 'cheb-end'):
            endpoints = True
        zeros = kwargs.pop("zeros", True)

        domain = get_limits(grid, zeros=zeros, endpoints=endpoints)
        cheby_results = is_grid_chebyshev(grid, domain)
        is_cheby = cheby_results['chebyshev']; cheby_type = cheby_results['type']
        e = [cheby_results['zero-error'], cheby_results['extrema-error'],
             cheby_results['zero-endpoints-error']]
        if (not is_cheby):
            print("\n---ERROR: grid doesn't appear to be chebyshev, error = {}\n".format(e))
            exit()

        if (not indefinite):
            integrator_args = {'zeros':zeros, 'domain':domain, 'endpoints':endpoints}
            integrator = chebyshev_integ
        else:
            integrator_args = {'zeros':zeros, 'domain':domain, 'x':grid, 'endpoints':endpoints}
            integrator = chebyshev_antideriv

    else:
        print("\n---ERROR: should never be here\n")
        exit()

    # integrate over the entire grid
    #     I = int_a^b f(x) dx = single number
    if (not indefinite):
        if ('cheb' in method):
            integral = integrator(data, **integrator_args)
        else:
            integral = integrator(data, grid, **integrator_args)

    # do indefinite integral:
    #     I(r) = int_a^r f(x) dx = array
    #     I(a) = 0
    else:
        if ('cheb' in method):
            integral = integrator(data, **integrator_args)

        elif (method == 'trap'):
            integral = integrator(data, grid, **integrator_args)

        elif (method == 'simps'):
            N = len(grid); integral = np.zeros_like(data)

            integral[0] = 0. # int_a^a f(x) dx = 0

            # integrate from x0 to x1, by fitting through x0,x1,x2
            delta10 = grid[1]-grid[0]; delta20 = grid[2]-grid[0]; delta21 = grid[2]-grid[1]
            f0 = data[0]; f1 = data[1]; f2 = data[2]
            det = delta10**2*delta20 - delta20**2*delta10
            Anum = f0*(delta10-delta20) + f1*delta20 - f2*delta10
            Bnum = f0*(delta20**2-delta10**2) + f2*delta10**2 - f1*delta20**2
            C    = f0
            A = Anum/det; B = Bnum/det
            integral[1] = A*(delta10**3)/3. + B*(delta10**2)/2. + C*delta10

            # all interior points
            for i in range(2, N):
                integral[i] = integrator(data[:i+1], grid[:i+1], **integrator_args)

        elif (method == '8th'):
            N = len(grid); integral = np.zeros_like(data)

            integral[0] = 0. # int_a^a f(x) dx = 0

            # fit first 5 data points
            d10 = grid[1] - grid[0]; d20 = grid[2] - grid[0]
            d30 = grid[3] - grid[0]; d40 = grid[4] - grid[0]
            f0 = data[0]; f1 = data[1]; f2 = data[2]; f3 = data[3]; f4 = data[4]
            gamma = [(f1-f0)/d10, (f2-f0)/d20, (f3-f0)/d30, (f4-f0)/d40]
            delta = 0.25*(abs(d10) + abs(d20) + abs(d30) + abs(d40))
            dt10 = d10/delta; dt20 = d20/delta; dt30 = d30/delta; dt40 = d40/delta
            NN = np.zeros((4,4))
            NN[0,:] = [dt10**3, dt10**2, dt10, 1.]
            NN[1,:] = [dt20**3, dt20**2, dt20, 1.]
            NN[2,:] = [dt30**3, dt30**2, dt30, 1.]
            NN[3,:] = [dt40**3, dt40**2, dt40, 1.]
            coeffs = linalg.solve(NN, gamma)
            A = coeffs[0]/delta**3; B = coeffs[1]/delta**2; C = coeffs[2]/delta
            D = coeffs[3]; E = f0

            # integrate from x0 to x1, by fitting through x0,x1,x2,x3,x4
            integral[1] = A*(d10**5)/5. + B*(d10**4)/4. \
                        + C*(d10**3)/3. + D*(d10**2)/2. + E*d10

            # integrate from x0 to x2, by fitting through x0,x1,x2,x3,x4
            integral[2] = A*(d20**5)/5. + B*(d20**4)/4. \
                        + C*(d20**3)/3. + D*(d20**2)/2. + E*d20

            # integrate from x0 to x3, by fitting through x0,x1,x2,x3,x4
            integral[3] = A*(d30**5)/5. + B*(d30**4)/4. \
                        + C*(d30**3)/3. + D*(d30**2)/2. + E*d30

            # integrate the rest of the points
            for i in range(4, N):
                integral[i] = integrator(data[:i+1], grid[:i+1], **integrator_args)

        if (data_reversed):
            integral = integral[::-1]

    return integral

def midpoint(fx, x, **kwargs):
    """
    2nd order accurate on uniform and non-uniform grids
    """
    N = len(x)
    result = fx[0]*(x[1]-x[0])
    for i in range(1,N-1):
        result += fx[i]*(x[i+1] - x[i-1])
    result += fx[N-1]*(x[N-1]-x[N-2])
    result *= 0.5
    return result

def trapezoid(fx, x, indefinite=False, **kwargs):
    """
    2nd order accurate on uniform and non-uniform grids
    """
    if (indefinite):
        return indef_trap(fx, x=x, initial=0, **kwargs)
    else:
        return trapezoid_integrate(fx, x=x, **kwargs)

def simpson(fx, x, **kwargs):
    """
    4th order accurate when using a UNIFORM grid, only 2nd order accurate
    on a Chebyshev non-uniform grid. fit parabola to three points --> 4th order
    """
    N = len(x); Nslabs = N-1
    if (Nslabs%2 == 0):
        # even number of slabs --> standard simpson integration
        add_last = False
        M = Nslabs
    else:
        # odd number of slabs --> need to add last slab
        add_last = True
        M = Nslabs-1
    # integrate the first even set of slabs
    result = 0.
    for i in range(0,M,2):
        delta10 = x[i+1] - x[i]; delta20 = x[i+2] - x[i]
        f0 = fx[i]; f1 = fx[i+1]; f2 = fx[i+2]
        delta = [delta10, delta20]; _f = [f0, f1, f2]
        det = delta10**2*delta20 - delta20**2*delta10
        Anum = f0*(delta10-delta20) + f1*delta20 - f2*delta10
        Bnum = f0*(delta20**2-delta10**2) + f2*delta10**2 - f1*delta20**2
        C    = f0
        A = Anum/det; B = Bnum/det
        result += A*delta20**3/3. + B*delta20**2/2. + C*delta20
    # integrate the last odd slab
    if (add_last):
        delta10 = x[-2] - x[-3]; delta20 = x[-1] - x[-3]; delta21 = x[-1] - x[-2]
        f0 = fx[-3]; f1 = fx[-2]; f2 = fx[-1]
        det = delta10**2*delta20 - delta20**2*delta10
        Anum = f0*(delta10-delta20) + f1*delta20 - f2*delta10
        Bnum = f0*(delta20**2-delta10**2) + f2*delta10**2 - f1*delta20**2
        C    = f0
        A = Anum/det; B = Bnum/det
        result +=   A*(delta20**3 - delta10**3)/3. \
                  + B*(delta20**2 - delta10**2)/2. \
                  + C*delta21
    return result

def eighth_order(fx, x, **kwargs):
    """
    8th order accurate when using a UNIFORM grid, only 6th order accurate
    on a Chebyshev non-uniform grid. fit quartic to five points --> 8th order

    relies on linalg.solve which is O(N**3). every iteration involes a
    4x4 solve and there are ~N/4 iterations. the total algorithm should
    cost about ~N/4*O(4**3) ~ 16*N ~ 10*O(N)
    """
    N = len(x)      # number of grid points
    Nslabs = N-1    # number of "rectangles" or slabs
    M = Nslabs // 4 # num of 5 pt sections that can use stencil, i.e., # of 4 slab sections
    extra_slabs = Nslabs - 4*M # extra slabs

    N = np.zeros((4,4)); gamma = np.zeros((4))
    result = 0.
    for i in range(0,4*M,4):
        # fit 5 points to quartic & integrate over the 4 slabs
        #   f(x)-f0 = A*(x-x0)**4 + B*(x-x0)**3 + C*(x-x0)**2 + D*(x-x0)
        #   (f(x)-f0)/(x-x0) = A*(x-x0)**3 + B*(x-x0)**2 + C*(x-x0) + D
        #   (f(x)-f0)/(x-x0) = At*(dtx0)**3 + Bt*(dtx0)**2 + Ct*(dtx0) + Dt
        #   dtx0 = (x-x0)/delta; At = A*delta**3; Bt = B*delta**2; Ct = C*delta; Dt = D
        d10 = x[i+1] - x[i]; d20 = x[i+2] - x[i]; d30 = x[i+3] - x[i]; d40 = x[i+4] - x[i]
        f0 = fx[i]; f1 = fx[i+1]; f2 = fx[i+2]; f3 = fx[i+3]; f4 = fx[i+4]

        # fill RHS
        gamma  = [(f1-f0)/d10, (f2-f0)/d20, (f3-f0)/d30, (f4-f0)/d40]

        # build matrix and apply preconditioning
        delta = 0.25*(abs(d10) + abs(d20) + abs(d30) + abs(d40))
        dt10 = d10/delta; dt20 = d20/delta; dt30 = d30/delta; dt40 = d40/delta
        N[0,:] = [dt10**3, dt10**2, dt10, 1.] # At
        N[1,:] = [dt20**3, dt20**2, dt20, 1.] # Bt
        N[2,:] = [dt30**3, dt30**2, dt30, 1.] # Ct
        N[3,:] = [dt40**3, dt40**2, dt40, 1.] # Dt

        coeffs = linalg.solve(N, gamma)

        # "undo" the matrix preconditioning
        A = coeffs[0]/delta**3; B = coeffs[1]/delta**2; C = coeffs[2]/delta
        D = coeffs[3]; E = f0

        # integrate from x0 to x4
        result += A*d40**5/5. + B*d40**4/4. + C*d40**3/3. + D*d40**2/2. + E*d40

    # integrate over the rest of the slabs, slabs with <=4 pts
    if (extra_slabs > 0):
        # fit last 5 points to quartic & integrate over last slab
        d10 = x[-4] - x[-5]; d20 = x[-3] - x[-5]; d30 = x[-2] - x[-5]; d40 = x[-1] - x[-5]
        f0 = fx[-5]; f1 = fx[-4]; f2 = fx[-3]; f3 = fx[-2]; f4 = fx[-1]
        gamma = [(f1-f0)/d10, (f2-f0)/d20, (f3-f0)/d30, (f4-f0)/d40]
        delta = 0.25*(abs(d10) + abs(d20) + abs(d30) + abs(d40))
        dt10 = d10/delta; dt20 = d20/delta; dt30 = d30/delta; dt40 = d40/delta
        N[0,:] = [dt10**3, dt10**2, dt10, 1.]
        N[1,:] = [dt20**3, dt20**2, dt20, 1.]
        N[2,:] = [dt30**3, dt30**2, dt30, 1.]
        N[3,:] = [dt40**3, dt40**2, dt40, 1.]
        coeffs = linalg.solve(N, gamma)
        A = coeffs[0]/delta**3; B = coeffs[1]/delta**2; C = coeffs[2]/delta
        D = coeffs[3]; E = f0

        if (extra_slabs == 1):
            # integrate x3 to x4
            result += A*(d40**5-d30**5)/5. + B*(d40**4-d30**4)/4. + C*(d40**3-d30**3)/3. \
                    + D*(d40**2-d30**2)/2. + E*(d40-d30)

        elif (extra_slabs == 2):
            # integrate x2 to x4
            result += A*(d40**5-d20**5)/5. + B*(d40**4-d20**4)/4. + C*(d40**3-d20**3)/3. \
                    + D*(d40**2-d20**2)/2. + E*(d40-d20)

        elif (extra_slabs == 3):
            # integrate x1 to x4
            result += A*(d40**5-d10**5)/5. + B*(d40**4-d10**4)/4. + C*(d40**3-d10**3)/3. \
                    + D*(d40**2-d10**2)/2. + E*(d40-d10)

        else:
            print("\n---ERROR: should never be here\n")
            exit()

    return result

