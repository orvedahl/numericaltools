"""
perform Forward/Reverse FFT using pyFFTW interface to FFTW

    --assumes FFTW is installed
    --assumes pyFFTW is installed

Usage:
    fftw.py [options]

Options:
    --case=<c>     Which test case to use [default: 1]
    --nx=<nx>      How many discrete data pts to use in test [default: 512]
    --log          Plot on a log scale [default: False]

R. Orvedahl 6-13-2017
"""

from __future__ import print_function
import numpy as np

def interp_make_uniform(data_in, t_in, domain=None):
    """
    interpolate data to a uniform grid suitable for FFT-ing

    --data_in is the function data
    --t_in is the "time" axis or space
    --domain is a list specifying the domain bounds
    """
    nt = len(t_in)
    if (domain == None): # make a guess
        tlo = t_in[0]; thi = t_in[-1]
    else:
        tlo = domain[0]; thi = domain[-1]

    t_uniform = np.linspace(tlo, thi, nt, endpoint=True)

    f_interp = scipy_interp.interp1d(t_in, data_in)
    data_uniform = f_interp(t_uniform)

    return t_uniform, data_uniform

def FFT_single_axis(Fx, th, window='none', th_axis=0):
    """
    perform FFT on one axis of 2D data

    th_axis = 0
       Fx(theta, r) --> Fk(ktheta, r)

    th_axis = 1
       Fx(r, theta) --> Fk(r, ktheta)
    """

    if (th_axis == 0):
        # Data is Fx(nth, nr)

        # get length of radial direction
        nr = len(Fx[0,:])

        # get length of frequency by FFT-ing the first radius
        fk0, kfreq, power0, xwin = FFT(Fx[:,0], th, window=window)
        nkth = len(fk0)

        # allocate space & fill in the first radius data
        Fk = np.zeros((nkth, nr), dtype=fk0.dtype)
        P = np.zeros((nkth, nr), dtype=power0.dtype)
        Fk[:,0] = fk0
        P[:,0] = power0

        # FFT the rest of the data
        for i in range(1,nr):
            Fk[:,i], kfreq, P[:,i], xwin = FFT(Fx[:,i], th, window=window)

    elif (th_axis == 1):
        # Data is Fx(nr, nth)

        # get length of radial direction
        nr = len(Fx[:,0])

        # get length of frequency by FFT-ing the first radius
        fk0, kfreq, power0, xwin = FFT(Fx[0,:], th, window=window)
        nkth = len(fk0)

        # allocate space & fill in the first radius data
        Fk = np.zeros((nr, nkth), dtype=fk0.dtype)
        P = np.zeros((nr, nkth), dtype=power0.dtype)
        Fk[0,:] = fk0
        P[0,:] = power0

        # FFT the rest of the data
        for i in range(1,nr):
            Fk[i,:], kfreq, P[i,:], xwin = FFT(Fx[i,:], th, window=window)

    else:
        raise NotImplementedError("FFT_single_axis can't FFT with axis = {}".format(axis))

    return Fk, kfreq, P, xwin

def lo_hi_freq(xx):
    """
    return lowest/highest freq that can be extracted from data
    """
    dx = np.mean(xx[1:]-xx[:-1])
    nx = len(xx)
    low_freq = 1./(dx*nx)
    nyquist_freq = 0.5/dx

    return low_freq, nyquist_freq, dx, nx

def restrict(low, nyq, kfreq, fk, power):
    """
    restrict quantities to physical frequencies
        i.e. f > Lowest && f < Nyquist
    """
    ind = np.where((kfreq >= low) & (kfreq <= nyq))
    return kfreq[ind], fk[ind], power[ind]

def restrict_single_axis(low, nyq, kfreq, fk, power):
    """
    restrict quantities to physical frequencies
        i.e. f > Lowest && f < Nyquist
    """
    ind = np.where((kfreq >= low) & (kfreq <= nyq))
    return kfreq[ind], np.squeeze(fk[ind,:]), np.squeeze(power[ind,:])

def get_window(xx, window):
    """
    return window function in physical space
    for more windows:
           https://en.wikipedia.org/wiki/Window_function
           where the following substitutions are used
               n   --> xx - cntr
               N-1 --> xx[-1] - xx[0]
    """
    if (window == None or window.lower() == 'none'):
        xwin = np.ones_like(xx)

    elif (window.lower() == 'hanning'):
        cntr = 0.5*(xx[0]+xx[-1])
        L = xx[-1]-xx[0]
        xwin = 0.5 + 0.5*np.cos(2*np.pi*(xx-cntr)/L)

    elif (window.lower() == 'blackman-harris'):
        a0 = 0.35875; a1 = 0.48829
        a2 = 0.14128; a3 = 0.01168
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        cntr = 0.5*(xx[0]+xx[-1])
        L = xx[-1]-xx[0]
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    elif (window.lower() == 'blackman-nuttall'):
        a0 = 0.3635819; a1 = 0.4891775
        a2 = 0.1365995; a3 = 0.0106411
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        cntr = 0.5*(xx[0]+xx[-1])
        L = xx[-1]-xx[0]
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    elif (window.lower() == 'nuttall'):
        a0 = 0.355768; a1 = 0.487396
        a2 = 0.144232; a3 = 0.012604
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        cntr = 0.5*(xx[0]+xx[-1])
        L = xx[-1]-xx[0]
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    else:
        print("\nERROR: window not supported: "+window)
        print()
        xwin = np.ones_like(xx)

    return xwin

def FFT(fx, xx, window='blackman-harris'):
    """
    transform to Fourier space
    """

    lo, nyq, dx, npts = lo_hi_freq(xx)

    xwindow = get_window(xx, window)

    # the 2 is since we neglect negative freqs
    norm = 2./npts
    fk = norm*np.fft.rfft(fx*xwindow)

    freq = np.fft.fftfreq(npts, d=dx)
    freq = freq[0:int(npts/2)+1]
    if (freq[-1] < 0): # if last frequency is negative, make it positive
        freq[-1] *= -1.0
    kfreq = 2.*np.pi*freq

    power = fk*np.conj(fk)

    return fk, kfreq, power, xwindow

def iFFT(fk, npts):
    """
    transform back into Physical space
    """

    norm = 2./npts
    fkinv = np.fft.irfft(fk/norm)

    return fkinv

def DCT(fx, xx, window='blackman-harris', axis=0):
    """
    transform to coefficient space using Discrete Cosine Transform
    """
    N = len(xx)
    #norm = None
    norm = 'ortho'
    dct_type = 2

    xwindow = get_window(xx, window)

    # forward transform
    alpha = sp.dct(fx*xwindow, type=dct_type, n=N, axis=axis, norm=norm)

    if (norm is None):
        alpha /= N
        alpha[0] *= 0.5

    return alpha

def iDCT(C, axis=-1):
    """
    inverse transform from coefficient space using Discrete Cosine Transform
    """
    N = len(C)
    #norm = None
    norm = 'ortho'
    dct_type = 2

    # inverse transform
    fx = sp.idct(C, type=dct_type, n=N, axis=axis, norm=norm)

    if (norm is None):
        fx /= 2.*N

    return fx

def test_FFT(case=1, nx=100000, log=False):

    window = 'none'
    #window = 'blackman-harris'
    #window = 'hanning'

    # set up data
    twopi = 2*np.pi

    print("\nFunc:")
    if (case == 1):
        x = np.arange(-2., 2., 4./nx)
        f1 = twopi*0.2 ; A1 = 1
        fx = A1*np.cos(f1*x)
        print("\tA*cos(w1*t)")
        print("\tA :",A1)
        print("\tw1:",f1)
        print("\tw1/twopi = f1:",f1/twopi)

    elif (case == 2):
        x = np.arange(0, 2*twopi, 2.*twopi/nx)
        f1 = twopi*0.5  ; A1 = 1
        f2 = twopi*0.75 ; A2 = 2
        fx = A1*np.cos(f1*x) + A2*np.cos(f2*x)
        print("\tA*cos(w1*t) + B*cos(w2*t)")
        print("\tA :",A1)
        print("\tB :",A2)
        print("\tw1:",f1)
        print("\tw2:",f2)
        print("\tw1/twopi = f1:",f1/twopi)
        print("\tw2/twopi = f2:",f2/twopi)

    elif (case == 3):
        x = np.arange(-2., 2., 4./nx)
        f1 = twopi*3. ; A1 = 2
        fx = A1*np.cos(f1*x)*np.exp(-np.pi*x*x)
        print("\tA*cos(w1*t)*exp(-pi*t**2)")
        print("\tA :",A1)
        print("\tw1:",f1)
        print("\tw1/twopi = f1:",f1/twopi)

    elif (case == 4):
        x = np.linspace(0., 0.75, nx)
        f1 = twopi*50. ; A1 = 1
        f2 = twopi*80. ; A2 = 0.5
        fx = A1*np.sin(f1*x) + A2*np.sin(f2*x)
        print("\tA1*sin(w1*t) + A2*sin(w2*t)")
        print("\tA1:",A1)
        print("\tA2:",A2)
        print("\tw1:",f1)
        print("\tw2:",f2)
        print("\tw1/twopi = f1:",f1/twopi)
        print("\tw2/twopi = f2:",f2/twopi)

    elif (case == 5):
        x = np.arange(0, 2*twopi, 2.*twopi/nx)
        f1 = twopi*5. ; A1 = 10
        f2 = twopi*3. ; A2 = 5
        f3 = twopi*1. ; A3 = 2.5
        fx = A1*np.cos(f1*x) + A2*np.cos(f2*x) + A3*np.cos(f3*x)
        print("\tA1*cos(w1*t) + A2*cos(w2*t) + A3*cos(w3*t)")
        print("\tA1:",A1)
        print("\tA2:",A2)
        print("\tA3:",A3)
        print("\tw1:",f1)
        print("\tw2:",f2)
        print("\tw3:",f3)
        print("\tw1/twopi = f1:",f1/twopi)
        print("\tw2/twopi = f2:",f2/twopi)
        print("\tw3/twopi = f3:",f3/twopi)

    elif (case == 6):
        x = np.arange(0, 1, 1./nx)
        f1 = twopi*5.  ; A1 = 1
        f2 = twopi*50. ; A2 = 0.5
        fx = A1*np.sin(f1*x) + A2*np.sin(f2*x)
        print("\tA1*sin(w1*t) + A2*sin(w2*t)")
        print("\tA1:",A1)
        print("\tA2:",A2)
        print("\tw1:",f1)
        print("\tw2:",f2)
        print("\tw1/twopi = f1:",f1/twopi)
        print("\tw2/twopi = f2:",f2/twopi)

    elif (case == 7):
        import Utilities.chebyshev as mycheb
        x,th = mycheb._zeros(nx)
        x = mycheb._to_ab_space(x, -0.5, 3.5)
        fx = np.log(1.+x)
        print("\tlog(1 + t)")

    elif (case == 8):
        import Utilities.chebyshev as mycheb
        x,th = mycheb._zeros(nx)
        x = mycheb._to_ab_space(x, -5., 5.)
        fx = x + x*x
        print("\tx + x**2")

    elif (case == 9):
        import Utilities.chebyshev as mycheb
        x,th = mycheb._zeros(nx)
        x = mycheb._to_ab_space(x, -2., 4.)
        fx = 1. / (5. + x)
        print("\t1 / (5 + x)")

    elif (case == 10):
        import Utilities.chebyshev as mycheb
        x,th = mycheb._zeros(nx)
        fx = np.sqrt(1. - x**2)
        print("\tsqrt(1 - x**2)")

    elif (case == 11):
        import Utilities.chebyshev as mycheb
        x,th = mycheb._zeros(nx)
        fx = np.arccos(x)
        print("\tarccos(x)")

    else:
        print("\n---ERROR: cases above 10 have not been coded\n")
        import sys
        sys.exit()

    # get freq info for discrete data
    lowest_freq, nyquist_freq, dx, nx = lo_hi_freq(x)
    print("\tLowest Freq : {}".format(lowest_freq))
    print("\tNyquist Freq: {}".format(nyquist_freq))

    # do FFT
    fk, kfreq, power, xwin = FFT(fx, x, window=window)
    #kfreq /= twopi
    f_orig = fx*xwin
    fx_back  = iFFT(fk, nx)

    if (False):
        kfreq, fk, power = restrict(lowest_freq, nyquist_freq, kfreq, fk, power)

    ind = np.argsort(power)
    power_sorted = power[ind]
    kfreq_sorted = kfreq[ind]
    print("\nHighest Power:")
    print("\tkfreq\tkfreq/twopi\tpower")
    for i in range(1,4):
        print('\t',kfreq_sorted[-i], kfreq_sorted[-i]/twopi, power_sorted[-i])

    print("\nNyquist Freq:",nyquist_freq)
    print("Lowest Freq:",lowest_freq)
    print("\nLargest x:", np.max(x))
    print("Smallest x:", np.min(x))
    print("\nMax/Min Re(Fk):",np.max(np.real(fk)), np.min(np.real(fk)))
    print("Max/Min Im(Fk):",np.max(np.imag(fk)), np.min(np.imag(fk)))
    print("Max/Min Power :",np.max(power), np.min(power))

    print("\nTest DCT...")
    f_orig_dct = fx*xwin
    fk_dct = DCT(fx, x, window=window)
    fx_back_dct = iDCT(fk_dct)
    print(type(f_orig_dct), type(fk_dct), type(fx_back_dct))
    print(np.shape(f_orig_dct), np.shape(fk_dct), np.shape(fx_back_dct))
    print(f_orig_dct[:4])
    print(fx_back_dct[:4])
    print()
    print(fk_dct[:4])

    import pylab as plt
    figs = {}

    # these are analytic for the DCT
    if (case in [7,8,9,10,11]):

        fk_dct = mycheb.cheb_coeffs(f_orig_dct, zeros=True)

        C = np.zeros_like(fk_dct)

        if (case == 7):
            # f(x) = log(1+x)
            C[0] = -np.log(2.)
            for i in range(1,len(C)):
                C[i] = -np.pi*(-1.0)**i / i
        elif (case == 8):
            # f(x) = x+x**2
            C[0] = 0.5
            C[1] = 1.0
            C[2] = 0.5
        elif (case == 9):
            # f(x) = 1 / (5 + x)
            a = 1./np.sqrt(6.)
            b = 5. + np.sqrt(24.)
            for i in range(len(C)):
                C[i] = (-1.)**i*a/b**i
            C[0] *= 0.5
        elif (case == 10):
            # f(x) = sqrt(1-x**2)
            for i in range(len(C)/2):
                two_i = int(2*i)
                C[two_i] = -4./np.pi/(4.*i*i - 1.)
            C[0] *= 0.5
        elif (case == 11):
            # f(x) = arccos(x)
            for i in range(1, len(C)/2):
                odd = int(2*i-1)
                C[odd] = -2./(2.*i-1.)**2
            C[0] = 0.5*np.pi

        print("------------------")
        print(C[:4])
        print(fk_dct[:4])


        fig = plt.figure(figsize=(16,8))
        ax1 = fig.add_subplot(2,1,1)
        ax1.plot(np.abs(fk_dct), color='r', label='Numeric')
        ax1.plot(np.abs(C),      color='b', label='Analytic')
        ax1.set_xscale('symlog')
        ax1.set_yscale('log')
        ax1.set_ylabel(r"$|a_k|$")
        ax1.set_xlabel(r"$k$")
        ax1.set_title("Coefficients")
        ax1.legend()
        ax2 = fig.add_subplot(2,1,2)
        ax2.plot(np.abs((fk_dct-C)/C), label='Rel Error', color='k', linestyle='--')
        ax2.plot(np.abs(fk_dct - C), label='Abs Error', color='k', linestyle='-')
        ax2.set_ylim(1.e-16, 1e2)
        ax2.set_xscale('symlog')
        ax2.set_yscale('log')
        ax2.set_ylabel("Error")
        ax2.set_xlabel(r"$k$")
        ax2.set_title("Error")
        ax2.legend()
        figs['DCT_coeff'] = fig

    fig = plt.figure(figsize=(16,8))
    ax1 = fig.add_subplot(1,1,1)
    ax1.plot(fk_dct**2, color='r', label='Power')
    ax1.set_xscale('symlog')
    ax1.set_yscale('log')
    ax1.set_ylabel(r"$|a_k|^2$")
    ax1.set_xlabel(r"$k$")
    ax1.set_title("Coefficient Power Spectrum")
    ax1.legend()
    figs['DCT_power'] = fig

    fig = plt.figure(figsize=(16,8))
    ax1 = fig.add_subplot(2,1,1)
    ax1.plot(x, f_orig_dct, color='r', label='Original')
    ax1.plot(x, fx_back_dct, color='b', label='Reverse FFT')
    ax1.plot(x, xwin, color='g', linestyle='--', label=window)
    ax1.set_ylabel("Function")
    ax1.set_xlabel("Time")
    ax1.set_title("Physical Space")
    ax1.legend()
    ax2 = fig.add_subplot(2,1,2)
    ax2.semilogy(x, np.abs((fx_back_dct-f_orig_dct)/f_orig_dct), label='Rel Error', color='b')
    ax2.semilogy(x, np.abs(fx_back_dct - f_orig_dct), label='Abs Error', color='r')
    ax2.set_ylabel("Error")
    ax2.set_xlabel("Time")
    ax2.set_title("Error")
    ax2.legend()
    figs['DCT'] = fig

    fig = plt.figure(figsize=(16,8))
    ax1 = fig.add_subplot(2,1,1)
    ax1.plot(x, f_orig, color='r', label='Original')
    ax1.plot(x, fx_back, color='b', label='Reverse FFT')
    ax1.plot(x, xwin, color='g', linestyle='--', label=window)
    ax1.set_ylabel("Function")
    ax1.set_xlabel("Time")
    ax1.set_title("Physical Space")
    ax1.legend()
    ax2 = fig.add_subplot(2,1,2)
    ax2.semilogy(x, np.abs((fx_back - f_orig)/f_orig), label='Rel Error', color='b')
    ax2.semilogy(x, np.abs(fx_back - f_orig), label='Abs Error', color='r')
    ax2.set_ylabel("Error")
    ax2.set_xlabel("Time")
    ax2.set_title("Error")
    ax2.legend()
    figs['Fx'] = fig

    fig = plt.figure(figsize=(16,8))
    ax1 = fig.add_subplot(3,1,1)
    if (log):
        ax1.semilogy(kfreq, np.real(fk), color='r', label='Re(Fk)')
    else:
        ax1.plot(kfreq, np.real(fk), color='r', label='Re(Fk)')
    ax1.set_ylabel("Re(Fk)")
    ax1.legend()
    ax2 = fig.add_subplot(3,1,2)
    if (log):
        ax2.semilogy(kfreq, np.imag(fk), color='b', label='Im(Fk)')
    else:
        ax2.plot(kfreq, np.imag(fk), color='b', label='Im(Fk)')
    ax2.set_ylabel("Im(Fk)")
    ax2.legend()
    ax3 = fig.add_subplot(3,1,3)
    if (log):
        ax3.semilogy(kfreq, power, color='g', label='Power')
    else:
        ax3.plot(kfreq, power, color='g', label='Power')
    ax3.set_ylabel("Power")
    ax3.set_xlabel(r"Wavenumber, $\omega=2\pi f$")
    ax3.legend()
    figs['Fk'] = fig

    for key in figs.keys():
        out = 'test_FFT_{}.png'.format(key)
        figs[key].savefig(out)
        print("saved: "+out)


if __name__ == "__main__":
    from docopt import docopt
    args = docopt(__doc__)
    test_FFT(case=int(args['--case']), nx=int(args['--nx']), log=args['--log'])

