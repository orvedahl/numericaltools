"""
Module for performing operations on signal data

Smooth:
    --filter noisy data in an attempt to smooth it
        accepted filters:
            boxcar, triang, blackman, hann, hamming, flat,
            bartlett, flattop, blackmanharris, nuttall

"""

from __future__ import print_function
import numpy as np
from scipy import signal as sp_sig

def smooth(data, window_length=None, window='hann'):
    """
    smooth data using window with requested size
    """

    data = np.asarray(data)
    if (window_length is None):
        window_length = max(5, int(0.01*len(data)))

    accepted = ["boxcar", "triang", "blackman", "hann", "hamming", "bartlett",
                "flattop", "blackmanharris", "nuttall", "flat"]

    # error trap
    if (window not in accepted):
        raise ValueError("signals.smooth: window must be in {}".format(accepted))
    if (data.ndim != 1):
        raise ValueError("signals.smooth: only accepts 1D arrays")
    if (data.size <= window_length):
        raise ValueError("signals.smooth: should have input vector > window size")
    if (window_length < 5):
        raise ValueError("signals.smooth: increase window size to >= 5 for best results")

    # create filter
    if (window == "flat"):
        w = np.ones((window_length))
    else:
        w = sp_sig.get_window(window, window_length)

    # extend the data so it is even about the first & last index
    # this should not duplicate the first or last index, just evenly reflect it
    data = np.r_[data[window_length:0:-1], data, data[-2:-window_length-2:-1]]

    # apply filter to data
    filtered = sp_sig.convolve(data, w, mode='same')/np.sum(w)

    # ensure output is same size as incoming data
    filtered = filtered[window_length:-window_length]

    return filtered

