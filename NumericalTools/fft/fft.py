"""
Module for performinf Forward/Reverse FFTs and DCTs

Supported options, default values listed in []:
    --window, what window to apply before FFT
    --axis, what axis to apply FFT, only works for 2D arrays

"""
from __future__ import print_function

from ..interpolation import interp

from sys import exit
import numpy as np
import scipy.fftpack as sp

def FFT(f, x, window='blackman-harris', angular=True, axis=0):
    """
    -forward transform assuming f is real
    -returns angular frequencies
    """
    if (len(np.shape(f)) > 1):
        return _FFT_single_axis(f, x, window=window, axis=axis)

    lo, nyq, dx, npts = lo_hi_freq(x, angular=False)
    xwin = get_window(x, window)

    norm = 2./npts # 2 is because we neglect negative freqs
    fk = norm*np.fft.rfft(f*xwin)

    freq = np.fft.fftfreq(npts, d=dx)
    freq = freq[0:int(npts/2)+1]
    freq = np.abs(freq[0:int(npts/2)+1]) # if any freq is negative, fix that
    if (angular):
        freq *= 2.*np.pi

    power = fk*np.conj(fk)

    return fk, freq, power, xwin

def iFFT(fk, npts=None):
    """
    inverse transform assuming f is real
    """
    if (npts is None):
        if (len(fk) % 2 == 0):
            npts = 2*len(fk) - 1
        else:
            npts = 2*len(fk) - 2
    norm = 2./npts
    fkinv = np.fft.irfft(fk/norm)
    return fkinv

def FFTc(fx, x, angular=True, window='blackman-harris', **kwargs):
    """
    complex forward transform
    """
    lo, nyq, dx, npts = lo_hi_freq(x, angular=False)
    xwin = get_window(x, window)

    fk = sp.fft(fx*xwin, **kwargs)

    freq = sp.fftfreq(npts, d=dx)
    if (angular):
        freq *= 2.*np.pi

    power = fk*np.conj(fk)

    return fk, freq, power, xwin

def FFT_dfdx(fx, period, order=1):
    """
    compute dfdx using Fourier transform
    order is the order of differentiation
    if order < 0, then integration occurs
    """
    return sp.diff(fx, order=order, period=period)

def iFFTc(fk, **kwargs):
    """
    complex inverse transform
    """
    fkinv = sp.ifft(fk, **kwargs)
    return fkinv

def DCT(f, x=None, type=2, window='none', norm=None):
    """
    forward discrete cosine transform
    norm is 'ortho' or None
    """
    f = np.asarray(f)
    if (window is None or window.lower() == 'none'):
        xwin = np.ones_like(f)
    elif (x is not None):
        # grid passed and window was specified, so do something about it
        x = np.asarray(x)
        xwin = get_window(x, window)
    else:
        # window was specified, but grid was not
        print("\n---ERROR: DCT, when window /= None, need to pass grid")
        print("\tcalling sequence should be DCT(fx, x, window=win)")
        exit()
    N = None
    if (norm == 'ortho' or norm is None):
        alpha = sp.dct(f*xwin, type=type, n=N, norm=norm)
    return alpha

def iDCT(C, type=2, norm=None):
    """
    inverse discrete cosince transform
    norm is 'ortho' or None
    """
    C = np.asarray(C)
    N = None
    fx = sp.idct(C, type=type, n=N, norm=norm)
    if (norm is None):
        fx /=  2.*len(C)
    return fx

def _FFT_single_axis(Fx, x, window='none', axis=0):
    """
    perform FFT on one axis of 2D data

    axis = 0
       Fx(x, y) --> Fk(kx, y)

    axis = 1
       Fx(x, y) --> Fk(x, ky)
    """

    if (axis == 0):
        n = len(Fx[0,:])

        # get length of frequency by FFT-ing the first entry
        fk0, kfreq, power0, xwin = FFT(Fx[:,0], x, window=window)
        nkx = len(fk0)

        # allocate space & fill in the first entry
        Fk = np.zeros((nkx, n), dtype=fk0.dtype)
        P  = np.zeros((nkx, n), dtype=power0.dtype)
        Fk[:,0] = fk0
        P[:,0]  = power0

        # FFT the rest of the data
        for i in range(1,n):
            Fk[:,i], kfreq, P[:,i], xwin = FFT(Fx[:,i], x, window=window)

    elif (axis == 1):
        n = len(Fx[:,0])

        # get length of frequency by FFT-ing the first radius
        fk0, kfreq, power0, xwin = FFT(Fx[0,:], x, window=window)
        nky = len(fk0)

        # allocate space & fill in the first radius data
        Fk = np.zeros((n, nky), dtype=fk0.dtype)
        P  = np.zeros((n, nky), dtype=power0.dtype)
        Fk[0,:] = fk0
        P[0,:]  = power0

        # FFT the rest of the data
        for i in range(1,n):
            Fk[i,:], kfreq, P[i,:], xwin = FFT(Fx[i,:], x, window=window)

    else:
        raise NotImplementedError("FFT can only handle 2D arrays, axis = {}".format(axis))

    return Fk, kfreq, P, xwin

def lo_hi_freq(xx, angular=False):
    """
    return lowest/highest freq that can be extracted from data
    if angular=True, return angular frequencies
    """
    dx = np.mean(xx[1:]-xx[:-1]); nx = len(xx)
    low_freq = 1./(dx*nx)
    nyquist_freq = 0.5/dx
    if (angular):
        twopi = 2.*np.pi
        return twopi*low_freq, twopi*nyquist_freq, dx, nx
    else:
        return low_freq, nyquist_freq, dx, nx

def interp_uniform(f, x, domain=None):
    """
    interpolate data to a uniform grid suitable for FFT-ing

    --f is the function data
    --x is the grid points
    --domain is a list specifying the domain bounds
    """
    n = len(x)
    if (domain is None):
        xlo = x[0]; xhi = x[-1]
    else:
        xlo = domain[0]; xhi = domain[-1]

    x_uniform = np.linspace(xlo, xhi, n, endpoint=False)

    interpolant = interp.interp(f, x)
    f_uniform = interpolant(x_uniform)

    return f_uniform, x_uniform

def restrict(low, nyq, freq, fk, power):
    """
    restrict quantities to physical frequencies
        i.e. f > Lowest && f < Nyquist
    """
    ind = np.where((freq >= low) & (freq <= nyq))
    return freq[ind], fk[ind], power[ind]

def get_window(xx, window):
    """
    return window function in physical space
    for more windows:
           https://en.wikipedia.org/wiki/Window_function
           where the following substitutions are used
               n   --> xx - cntr
               N-1 --> xx[-1] - xx[0]
    """
    L = xx[-1] - xx[0]; cntr = 0.5*(xx[0] + xx[-1])

    if (window is None or window.lower() == 'none'):
        xwin = np.ones_like(xx)

    elif (window.lower() == 'hanning'):
        xwin = 0.5 + 0.5*np.cos(2*np.pi*(xx-cntr)/L)

    elif (window.lower() == 'blackman-harris'):
        a0 = 0.35875; a1 = 0.48829
        a2 = 0.14128; a3 = 0.01168
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    elif (window.lower() == 'blackman-nuttall'):
        a0 = 0.3635819; a1 = 0.4891775
        a2 = 0.1365995; a3 = 0.0106411
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    elif (window.lower() == 'nuttall'):
        a0 = 0.355768; a1 = 0.487396
        a2 = 0.144232; a3 = 0.012604
        b1 = 2.*np.pi; b2 = 4.*np.pi; b3 = 6.*np.pi
        xwin = -(a0 - a1*np.cos(b1*(xx-cntr)/L) \
                  + a2*np.cos(b2*(xx-cntr)/L) \
                  - a3*np.cos(b3*(xx-cntr)/L)) + 1.
        xwin /= np.max(xwin)

    else:
        print("\nERROR: window not supported: "+window)
        print()
        xwin = np.ones_like(xx)

    return xwin

