"""
setup file
"""
from __future__ import print_function
from setuptools import setup, find_packages

setup(
    name='NumericalTools',
    version='2',
    author='Ryan J. Orvedahl',
    author_email='ryan.orvedahl@gmail.com',
    license='GPL3',
    description='Suite of numerical math tools',
    long_description=open('README').read(),
    packages=find_packages(),
    install_requires=["numpy", "scipy", "matplotlib", "h5py", "docopt"] #, "pyfftw"]
     )
