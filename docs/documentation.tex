%
% Ryan Orvedahl Jul 2017
%

\documentclass[12pt]{article}

% Define new commands (shortcuts):
\input shortcuts

% change margins
\usepackage{geometry}
\geometry{left=1in, right=1in}

% colors and the \hl{} command to highlight stuff
\usepackage{color}

% cool math stuff and bold typesetting
\usepackage{amsmath, bm}

% special colors and definitions for framing source code listings
\usepackage{listings}
\definecolor{AntiqueWhite3}{rgb}{0.804,0.753,0.69}
\definecolor{DarkerAntiqueWhite3}{rgb}{0.659,0.635,0.612}
\definecolor{orange}{rgb}{1.0,0.65,0.0}
\lstset{%
  keywordstyle=\color{blue}\ttfamily,%
  commentstyle=\itshape\color[gray]{0.5},%
  mathescape=true,%
  basicstyle=\small\ttfamily,%
  %frameround=fttt,%
  frameround=ffff,%
  %frame=shadowbox,%
  frame=single,%
  rulesepcolor=\color{DarkerAntiqueWhite3},%
  backgroundcolor=\color{AntiqueWhite3},%
  emph={load,add_slice,save}, emphstyle=\color{blue},%
  emph={[2]In}, emphstyle=[2]\color{yellow},%
  emph={[3]Out}, emphstyle=[3]\color{orange},%
  xleftmargin=0.5em,
  xrightmargin=0.5em,
  showstringspaces=false,
  breaklines=true,
  mathescape=false}


\begin{document}

%========================================================================
% create title and author
%========================================================================
\title{\numtools\ Documentation}
\author{Ryan Orvedahl}
\date{August 2, 2017}

\maketitle

\tableofcontents

%========================================================================
% introduction
%========================================================================
\section{Introduction}
\numtools\ is a suite of (you guessed it) numerical tools written in Python
to perform various mathematical operations. They were originally designed with
Python 2, but should work without any issues under Python 3.

\subsection{Getting the Source Code}
\numtools\ is available through Bitbucket on a {\sf git} repository. To download
the source code, run
\begin{lstlisting}[language=bash,mathescape=true]
> git clone https://bitbucket.org/orvedahl/NumericalTools.git
\end{lstlisting}
this will download the source code into a directory called {\tt NumericalTools}.
Note that the command is made up of three distinct strings separated by spaces;
{\tt `git'}, {\tt `clone'}, and the {\tt `https://...'} part. There are no spaces or
carriage returns in the {\tt `https://...'} part of the command.

\subsection{Installing the Python Source}
First, change directories to the \numtools\ source directory:
\begin{lstlisting}[language=bash,mathescape=true]
> cd NumericalTools
\end{lstlisting}
Next, take notice of the {\tt README}, {\tt INSTALL}, and {\tt INSTALL-DEVEL}
files. These will give you a breif overview of the package and provide detailed,
step-by-step instructions for installing the code. We will reproduce the
standard installation steps here.

\subsubsection{Standard Installation}
This is the most common installation method. It will install any dependencies
that are required using the Python tool {\sf pip}. The list of dependencies
that were installed will be written to the file {\tt ``installed.txt''}.
The source code will be installed to a local directory that {\bf does not}
require root access. Typically the location is something like
{\tt ``\$HOME/.local/''}. This location is already included in the {\tt PYTHONPATH},
so no extra environment setup is required.

To install the source simply execute:
\begin{lstlisting}[language=bash,mathescape=true]
> bin/install.sh .
\end{lstlisting}
Notice that there is a trailing {\tt ``.''} which indicates the current working
directory is the home directory of \numtools.

If you decide to modify any of the source code, \numtools\ will need to be
reinstalled before the changes will take place. This is done simply as
\begin{lstlisting}[language=bash,mathescape=true]
> cd /path/to/NumericalTools/
> bin/reinstall.sh .
\end{lstlisting}

\subsubsection{Test the Installation}
Enter Python by executing:
\begin{lstlisting}[language=bash,mathescape=true]
> python
\end{lstlisting}
this will bring you into Python and should produce some kind of welcome message.
Now import \numtools:
\begin{lstlisting}[language=bash,mathescape=true]
>>> import NumericalTools
\end{lstlisting}
If it appears that nothing happened and you were returned to the
``\textgreater\textgreater\textgreater'' prompt,
that indicates a successful installation. If any errors were produced, then
the installation may have failed in some way.

Running the following command
\begin{lstlisting}[language=bash,mathescape=false]
>>> NumericalTools.tests.__all__
\end{lstlisting}
will list all the Python files that are in the testing directory. The important
files will be named {\tt ``test\_something''}. To run that test, execute
\begin{lstlisting}[language=bash,mathescape=false]
>>> NumericalTools.tests.test_something.run()
\end{lstlisting}
For example to test the Chebyshev routines and then the integration routines:
\begin{lstlisting}[language=bash,mathescape=false]
>>> NumericalTools.tests.test_chebyshev.run()
>>> NumericalTools.tests.test_integrate.run()
\end{lstlisting}

\subsection{Uninstalling the Source}
To uninstall the source code, first switch to the \numtools\ source directory:
\begin{lstlisting}[language=bash,mathescape=true]
> cd NumericalTools
\end{lstlisting}
Use the {\tt ``uninstall.sh''} script to remove all the installed dependencies:
\begin{lstlisting}[language=bash,mathescape=true]
> bin/uninstall.sh .
\end{lstlisting}
Notice the trailing {\tt ``.''} indicating the home directory of \numtools\ is
the current working directory.
Then you can move out of the \numtools\ source directory and remove the \numtools\
directory:
\begin{lstlisting}[language=bash,mathescape=true]
> cd ..
> rm -rf NumericalTools
\end{lstlisting}
At this point, no trace of the \numtools\ package should exist on your system.
This includes any dependencies that were installed as requirements for \numtools.

%========================================================================
% list of every module
%========================================================================
\section{Included Modules}

\subsection{Calculus}

\subsubsection{Differentiation}
This module provides routines for calculating derivatives in one dimension.
There is support for uniform grids, non-uniform grids, and chebyshev grids.
The main routine has the basic calling structure
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import differentiate
dFdx = differentiate.derivative(Fx, x)
\end{lstlisting}

\noindent
Options (and their defaults) that affect any differentiation method include:
\begin{itemize}
 \item data\_reversed = False, is the grid monotonically increasing/decreasing
 \item method = `fd', what method to use: fd, cfd, fourier, cheb, cheb-end
\end{itemize}

\noindent
These options are only relevant for the `fd' method:
\begin{itemize}
 \item order = 6, what finite difference order to use: 2, 4, 6
\end{itemize}

\noindent
These options are only relevant for the `cheb' method:
\begin{itemize}
 \item zeros = True, is it a chebyshev zeros grid or not
 \item matrix = False, use the differentiation matrix method
 \item endpoints = False, does the chebyshev zeros grid include the endpoints
\end{itemize}

\noindent
These options are only relevant for the `fourier' method:
\begin{itemize}
 \item window = None, specify a window type
\end{itemize}

\subparagraph{General Remarks}
If the grid is uniform, it is best to use either the 6th order finite
difference method (`fd') or the 6th order compact finite difference method
(`cfd'). Those will be the most accurate and fastest. The fourier method
is in theory more accurate, but it has not been extensively tested, so
use it with caution. It also requires the function to be periodic on the
domain.

When the grid is a chebyshev zeros grid, the best option is to use the
matrix=False chebyshev method. This will invoke the Discrete Cosine Transform.
If the grid is a chebyshev extrema grid, a Fast Fourier Transform will be used.

If you do not know what the grid is, or it is not uniform and not a chebyshev
grid, then the best bet is 6th order finite difference.

\subsubsection{Integration}
This module provides routines for calculating integrals in one dimension.
There is support for definite and indefinite integrals.
The main routine has the basic calling structure
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import integrate
integral = integrate.integrate(Fx, x)
\end{lstlisting}

\noindent
Options (and their defaults) that affect any integration method include:
\begin{itemize}
 \item data\_reversed = False, is the grid monotonically increasing/decreasing
 \item method = `trap', what method to use: trap, cheb, simps, 8th, cheb-end
 \item indefinite = False, calculate a definite or indefinite integral
\end{itemize}

\noindent
These options are only relevant for the `cheb' method:
\begin{itemize}
 \item zeros = True, is it a chebyshev zeros grid or not
 \item endpoints = False, does the chebyshev zeros grid include the endpoints
\end{itemize}

\subparagraph{General Remarks}
If the grid is uniform, the 8th order method will be the best.
When using a nonuniform grid, every method seems to reduce to only 2nd order
even when they have been derived assuming a nonuniform grid. So if the grid
is not chebyshev, it is best to just use the trap method.

\subsection{Fourier Transforms}

\subsubsection{FFT}
This module provides routines for calculating Fourier Transforms in one dimension.
If the incoming data has only two dimensions, we can transform one of them, but
not if the data is three or more dimensions (in progress).
It assumes the incoming data is real. It further assumes the incoming grid is
uniform and that the data is periodic on the grid.
The main routine has the basic calling structure
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import fft
Fk, freq, P, xwin = fft.FFT(Fx, x)
\end{lstlisting}
where {\tt Fk} is the Fourier transformed data, {\tt freq} is the frequencies,
{\tt P} is the power and {\tt xwin} is the window function in physical space
that was applied. The inverse transform would be
\begin{lstlisting}[language=python,mathescape=true]
Fkinv = fft.iFFT(Fk)
\end{lstlisting}

\noindent
Options for the forward transform include:
\begin{itemize}
 \item window = `blackman-harris', what kind of window to apply
 \item angular = True, are the angular frequencies returned or not
 \item axis = 0, what axis to FFT, only supported when data is 2D
\end{itemize}

\subparagraph{General Remarks}
If the incoming data is not on a uniform grid, there is a method for
interpolating to a uniform grid. It is pretty much a basic wrapper
to the routines of the Interpolation module. It is called as
\begin{lstlisting}[language=python,mathescape=true]
F_uniform, x_uniform = fft.interp_uniform(Fx, x)
\end{lstlisting}

\subsubsection{Signals}
This routine is located under the FFT directory. It provides methods that smooth
high frequency data. The main routine has the basic calling structure
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import signals
filtered_F = signals.smooth(F)
\end{lstlisting}

Options include:
\begin{itemize}
 \item window = `hann', what kind of window to apply
 \item window\_length = None, what size is the window
\end{itemize}

\subparagraph{General Remarks}
The larger the length of the window, the smoother the data will appear.

\subsection{Filesystem}
This module includes routines that read/write data to files using HDF5, manipulate
filenames, and perform tree walks.

When manipulating filenames, the extension can be changed and text can be inserted
between the basename and the new extension.
\begin{lstlisting}[language=python,mathescape=false]
from NumericalTools import file_io
newfilename = file_io.outputname(filename,
                                 ext=`.png',
                                 insert=`devel')
\end{lstlisting}

A tree walk will return all files in the given directory. You can exclude any
files with a given extension and you can exclude directories. For example,
to find all Fortran files and excluding the {\tt ``build''} directory:
\begin{lstlisting}[language=python,mathescape=true]
files = file_io.treewalk(top_directory,
                         include_ext=[`f90', `F90'],
                         exclude_dirs=[`build'])
\end{lstlisting}

When writing data to a file using HDF5, the data is supplied as {\tt key=value}
entries. Each {\tt key} specifies the name of the data and the {\tt value} holds
the actual data. For example, to save time and data information that are
stored in the arrays {\tt t} and {\tt vals}, respectively:
\begin{lstlisting}[language=python,mathescape=true]
file_io.save_data(filename, data=vals, time=t)
\end{lstlisting}
Saving data in this way only works with simple data types. Arrays, scalars,
and strings all work fine, dictionaries and lists may not work. There is
no limit to how many {\tt key=value} pairs that can be saved, although there
may be a filesystem imposed limit if the file size gets too large.

To retrieve data that was stored in this way:
\begin{lstlisting}[language=python,mathescape=true]
data, data_keys = file_io.save_data(filename)
vals = data[`data']
t = data[`time']
\end{lstlisting}

\subparagraph{General Remarks}
When saving data with the {\tt save\_data} routine, the default is no compression.
To enable compression, include the keyword {\tt compress=True}.

\subsection{Fitting}
These routines help with fitting models to data. They all make use of a
nonlinear least squares algorithm. A few different ways to calculate
the error in the model parameters are included.

\subparagraph{General Remarks}
This code is under active development, see source code for the calling
sequence and options.

\subsection{Interpolation}
These routines provide different ways of interpolating discrete data
The main routine has the basic calling structure
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import interp
F = interp.interp(f_data, x_data)
interp_F = F(x_fine)
\end{lstlisting}
Notice that the {\tt interp.interp} function returns a callable function.

\noindent
Options include:
\begin{itemize}
 \item method = `rbf', what method to use: rbf, linear, nearest, zero, slinear,
       quadratic, cubic
\end{itemize}

\noindent
When using the default `rbf' method, the following options are available:
\begin{itemize}
 \item func = `quintic', function to use: gaussian, multiquadric, linear, cubic
              quintic, thin-plate, inverse
 \item epsilon = None, shape parameter
\end{itemize}

\subparagraph{General Remarks}
The cubic method is 4th order, quadratic is 3rd order, quinctic RBF is 6th order.

\subsection{Plotting}
These routines are just simple helper routines. The two most important routines
are {\tt get\_color} and {\tt plot2d}.

Given a number {\tt x} in the range {\tt [xmin, xmax]}, convert that to a color
using the provided colormap:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import plotting
color = plotting.get_color(x,
                           xmin=xmin,
                           xmax=xmax,
                           cmap=`jet')
\end{lstlisting}

{\tt plot2d} is a wrapper to {\tt matplotlib}'s {\tt pcolormesh}. The x and y
axes are given by 1d arrays and the data to plot can be in {\tt data(x,y)} order
\begin{lstlisting}[language=python,mathescape=true]
image = plotting.plot2d(x, y, data, order=`xy')
\end{lstlisting}
or {\tt data(y,x)} order:
\begin{lstlisting}[language=python,mathescape=true]
image = plotting.plot2d(x, y, data, order=`yx')
\end{lstlisting}
Notice that the order of {\tt x} and {\tt y} are the same for both calling
sequences. Any other options that are passed to {\tt plot2d} will be passed to
{\tt pcolormesh}.

\subsection{Polynomials}

\subsubsection{Chebyshev}
This module provides routines to deal with Chebyshev polynomials, $T_n$. It includes
expanding a function in terms of $T_n$, differentiation, definite and indefinite
integration, and many helper routines.

Given function values at the Chebyshev grid points, a Chebyshev instance can be
initialized as
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import chebyshev
cheb = chebyshev.Chebyshev(fx)
\end{lstlisting}

\noindent
Other options include
\begin{itemize}
 \item a = -1.0, the lower bound of the domain
 \item b =  1.0, the upper bound of the domain
 \item zeros = True, is the grid based on the zeros or the extrema
 \item endpoints = False, are the endpoints included in the zeros grid
\end{itemize}
The last option ({\tt endpoints}) needs some explanation. The default
Chebyshev zeros grid does not include the endpoints and uses the linear
transformation
\begin{equation}
\nonumber
x \in (-1,1) \longleftrightarrow r \in (a,b).
\end{equation}
Notice that the endpoints $a$ and $b$ are {\it not} included.
When the {\tt endpoints} option is True, a different linear scaling is used
that maps
\begin{equation}
\nonumber
x \in (-1,1) \longleftrightarrow r \in [a,b],
\end{equation}
where the enpoints $a$ and $b$ {\it are} included. Note that in both cases
$x$ does not include the boundary values of $-1$ or $+1$.

The Chebyshev class supports evaluation at a point, differentiation,
definite integration, and indefinite integration as
\begin{lstlisting}[language=python,mathescape=true]
Fx = cheb(x)
dFdx = cheb.deriv(x)
def_int = cheb.integ()
integral = cheb.antideriv(x)
\end{lstlisting}
where {\tt x} is where you want to evaluate the quantity, it need not be a
grid point.

There are also routines that make use of the Chebyshev class, but the user
never has to interact with the class:
\begin{lstlisting}[language=python,mathescape=true]
dFdx = chebyshev.derivative(fx, x, domain)
def_int = chebyshev.integral(fx, domain)
integral = chebyshev.antiderivative(fx, x, domain)
\end{lstlisting}
where {\tt x} is where you want to evaluate the quantity, it need not be a
grid point. {\tt domain} is a list specifying the domain bounds.

There are two main helper routines for dealing with Chebyshev data.
The first one returns the bounds of the domain given the grid points. This is
trivial in the extrema case, but involves a linear solve in the roots case.
\begin{lstlisting}[language=python,mathescape=true]
a, b = chebyshev.get_limits(x, zeros=True, endpoints=False)
\end{lstlisting}
The second routine determines whether or not the grid is a Chebyshev grid,
what type of grid it is (zeros or extrema), and if the endpoints are included.
\begin{lstlisting}[language=python,mathescape=true]
results = chebyshev.is_grid_chebyshev(grid, domain)
\end{lstlisting}
where {\tt results} is a dictionary with the following keys:
\begin{itemize}
 \item chebyshev = True/False, is the grid a Chebyshev grid
 \item type = zeros or extrema, the type of grid
 \item endpoints = True/Fales, does the grid include the endpoints
 \item zero-error = float, max error in the traditional Chebyshev zeros grid
 \item extrema-error = float, max error in the Chebyshev extrema grid
 \item zero-endpoints-error = float, max error in the zeros grid that includes the endpoints
\end{itemize}

\subparagraph{General Remarks}
These routies were developed for a Chebyshev {\bf zeros} grid with support for the
extrema grid being an add-on later. As a result, the routines are not as
extensively tested for the extrema grid, but should still work without any major
issues.

\subsubsection{Legendre}
This module provides routines to deal with Legendre polynomials, $P_n$. It includes
expanding a function in terms of $P_n$, differentiation, definite and indefinite
integration, and many helper routines.

Given function values at the Legendre grid points, a Legendre instance can be
initialized as
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import legendre
leg = legendre.Legendre(fx)
\end{lstlisting}

\noindent
Other options include
\begin{itemize}
 \item a = -1.0, the lower bound of the domain
 \item b =  1.0, the upper bound of the domain
 \item endpoints = False, are the endpoints included in the zeros grid
\end{itemize}
The last option ({\tt endpoints}) needs some explanation. The default
Legendre zeros grid does not include the endpoints and uses the linear
transformation
\begin{equation}
\nonumber
x \in (-1,1) \longleftrightarrow r \in (a,b).
\end{equation}
Notice that the endpoints $a$ and $b$ are {\it not} included.
When the {\tt endpoints} option is True, a different linear scaling is used
that maps
\begin{equation}
\nonumber
x \in (-1,1) \longleftrightarrow r \in [a,b],
\end{equation}
where the enpoints $a$ and $b$ {\it are} included. Note that in both cases
$x$ does not include the boundary values of $-1$ or $+1$.

The Legendre class supports evaluation at a point, differentiation,
definite integration, and indefinite integration as
\begin{lstlisting}[language=python,mathescape=true]
Fx = leg(x)
dFdx = leg.deriv(x)
def_int = leg.integ()
integral = leg.antideriv(x)
\end{lstlisting}
where {\tt x} is where you want to evaluate the quantity, it need not be a
grid point.

There are also routines that make use of the Legendre class, but the user
never has to interact with the class:
\begin{lstlisting}[language=python,mathescape=true]
dFdx = legendre.derivative(fx, x, domain)
def_int = legendre.integral(fx, domain)
integral = legendre.antiderivative(fx, x, domain)
\end{lstlisting}
where {\tt x} is where you want to evaluate the quantity, it need not be a
grid point. {\tt domain} is a list specifying the domain bounds.

There is only one helper routine for dealing with Legendre data.
It determines whether or not the grid is a Legendre grid
and if the endpoints are included.
\begin{lstlisting}[language=python,mathescape=true]
results = legendre.is_grid_legendre(grid, domain)
\end{lstlisting}
where {\tt results} is a dictionary with the following keys:
\begin{itemize}
 \item legendre = True/False, is the grid a Legendre grid
 \item endpoints = True/Fales, does the grid include the endpoints
 \item error = float, max error in the traditional Legendre grid
 \item endpoints-error = float, max error in the Legendre grid that includes the endpoints
\end{itemize}

\subparagraph{General Remarks}
The integration and definite integration routines show spectral convergence out to
a large number of grid points. The differentiation routines show spectral convergence
at first, but then the error quickly grows as roughly fourth order. A fix should be
coming soon.

\subsection{Misc}
These are miscellaneous codes that do not belong under the other modules.

\subsubsection{Parsing}
This is a command line parsing tool. It is essentially a wrapper
to the {\tt docopt} python package. This should not be needed as {\tt docopt} is
installed as part of running the {\tt setup.py} script. It is still included so that
other parsing routines can easily be added such as {\tt getopt}, {\tt argparse},
or a custom routine.

Using the {\tt docopt} version is quite simple. If the python code has a docstring
(a three-quote string at the beginning of the file) that looks like:
\begin{lstlisting}[language=python,mathescape=true]
"""
This code does some magical task.

Usage:
    magic.py [options]

Options:
    --debug         Run in debug mode [default: False]
    --smoke=<s>     Use smoke level <s> [default: high]
    --mirrors=<m>   Use <m> mirrors [default: 2]
"""
\end{lstlisting}
Then the command line arguments {\tt debug}, {\tt smoke}, and {\tt mirrors} can
be parsed by running:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import parser
args = parser.parseArgs(__doc__)
main_program(**args)
\end{lstlisting}
The return argument from the {\tt parser} function is a dictionary of {\tt key=value}
pairs. For example, if no options were passed all elements of {\tt args} would
be the default values:
\begin{lstlisting}[language=python,mathescape=true]
args = {`--debug':False, `--smoke':`high', `--mirrors':2}
\end{lstlisting}
Notice that the {\tt `--'} is included in the key.

To see an example of this in action, all the test routines in the
{\tt NumericalTools/tests} directory use this functionality. Command line options
are only parsed and used when the code is run from the command line as any one of
the following
\begin{lstlisting}[language=python,mathescape=true]
> python magic.py
> python magic.py --smoke=low
> python magic.py --smoke=high --mirrors=4
> python magic.py --debug
\end{lstlisting}
When run in this way, the value of the variable {\tt `\_\_name\_\_'} will be equal
to {\tt `\_\_main\_\_'}. All of the test routines contain the code
\begin{lstlisting}[language=python,mathescape=true]
if __name__ == ``__main'':
    from NumericalTools import parser
    args = parser.parseArgs(__doc__)
    run(**args)
\end{lstlisting}
{\tt run} is the main function that should be executed. It can accept many arguments
when it is called and they are unpacked by using the double star notation.

\subsubsection{Grids}
This module provides easy access to various types of grids. Currently there are
only four supported grids: {\tt Chebyshev}, {\tt Legendre}, {\tt Uniform}, and {\tt Random}.
Their calling sequence looks like
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools.misc import grids
Grid0 = grids.LegendreGrid(N, a=xlo, b=xhi, zeros=True, endpoints=False)
Grid1 = grids.ChebGrid(N, a=xlo, b=xhi, zeros=True, endpoints=False)
Grid2 = grids.UniformGrid(N, a=xlo, b=xhi, endpoints=True)
Grid3 = grids.RandomGrid(N, a=xlo, b=xhi, seed=43)
\end{lstlisting}
The actual grid points are accessed as
\begin{lstlisting}[language=python,mathescape=true]
grid_points = Grid1.x
\end{lstlisting}
For the Chebyshev \& Legendre grids there is access to the transformed grid and the
angular grid (only Chebyshev)
\begin{lstlisting}[language=python,mathescape=true]
leg_grid_points = Grid0.xab
cheb_grid_points = Grid1.xab
cheb_theta_grid = Grid1.theta
\end{lstlisting}
and two supporting routines to transform between $(-1,1)$ and $[a,b]$
\begin{lstlisting}[language=python,mathescape=true]
x = Grid1.from_ab(r)
r = Grid1.to_ab(x)
\end{lstlisting}

Each grid has the {\tt info} attribute accessed as
\begin{lstlisting}[language=python,mathescape=true]
meta_data = Grid1.info
\end{lstlisting}
It is a dictionary where the keys are possible attributes and the values are short
descriptions of the quantity.

%\subsection{Devel}
%These are routines that are in development and not ready for general use. Typically no
%test function has been written for them and they might not even do what they are
%supposed to do. This is more of a scratch location to test out new ideas, algorithms,
%and functionalities.

%========================================================================
% examples of how to use it
%========================================================================
\section{Examples}
For the purpose of this example section, I will assume the data is from the \rayleigh\
pseudo-spectral code (that code was a main motivator for the development of the entire
\numtools\ package). In that case, the radial grid is a Chebyshev zeros grid that
\emph{includes} the endpoints. Let's use the kinetic energy as our data set and suppose
we want to take a volume average at a point given by
\begin{equation}
\left<\mathrm{KE}\right> = \frac{\int \mathrm{KE}(r,\theta,\phi) r^2\sin\theta
                                 \mathrm{d}r\mathrm{d}\theta\mathrm{d}\phi}
                                 {\int r^2\sin\theta
                                 \mathrm{d}r\mathrm{d}\theta\mathrm{d}\phi}.
\end{equation}
Let's also calculate the time derivative of the radial velocity $v_r$.

\subsection{Importing \numtools}
There are a few ways you can import the various modules of \numtools\ into your code.

a) you can import the entire package:
\begin{lstlisting}[language=python,mathescape=true]
import NumericalTools
\end{lstlisting}

b) individual modules can be imported using:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import differentiate, integrate
\end{lstlisting}

c) import a specific function from a particular module:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools.calculus.integrate import volume_avg
from NumericalTools.calculus.differentiate import derivative
\end{lstlisting}

d) there is also a public interface to common functions:
\begin{lstlisting}[language=python,mathescape=true]
import NumericalTools.public as NT
\end{lstlisting}

\subsection{Concrete Example 1}
Now we actually calculate the volume average and time derivative. First, we generate
our own radial grid that uses the zeros and includes the endpoints. Note, that the
radial grid of \rayleigh\ is reversed. The grid that comes from the {\tt ChebGrid}
class is not reversed, so we manually reverse it before the integration and use
the {\tt data\_reversed} keyword. Also note that boolean values in python can be
specified using either {\tt True/False} or the integers {\tt 1/0}.
There were many different ways of importing the functions so I present each way to
import the function followed by the sample function call.

a) importing the full package:
\begin{lstlisting}[language=python,mathescape=true]
import NumericalTools
radius = NumericalTools.misc.grids.ChebGrid(N,
                                            a=r_lo,
                                            b=r_hi,
                                            zeros=True,
                                            endpoints=True)
r = radius.xab[::-1]
vol_avg = NumericalTools.calculus.integrate.volume_avg(KE, r,
                                                method=`cheb-end',
                                                data_reversed=1)
dVdt = NumericalTools.calculus.differentiate.derivative(vr, time)
\end{lstlisting}

b) importing individual modules:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools import differentiate, integrate, grids
radius = grids.ChebGrid(N, a=r_lo, b=r_hi,
                        zeros=True, endpoints=True)
r = radius.xab[::-1]
vol_avg = integrate.volume_avg(KE, r,
                               method=`cheb-end', data_reversed=1)
dVdt = differentiate.derivative(vr, time)
\end{lstlisting}

c) importing specific functions from individual modules:
\begin{lstlisting}[language=python,mathescape=true]
from NumericalTools.calculus.integrate import volume_avg
from NumericalTools.calculus.differentiate import derivative
from NumericalTools.misc.grids import ChebGrid
radius = ChebGrid(N, a=r_lo, b=r_hi, zeros=True, endpoints=True)
r = radius.xab[::-1]
vol_avg = volume_avg(KE, r, method=`cheb-end',
                     data_reversed=1)
dVdt = derivative(vr, time)
\end{lstlisting}

d) using the public interface to common functions:
\begin{lstlisting}[language=python,mathescape=true]
import NumericalTools.public as NT
radius = NT.ChebGrid(N, a=r_lo, b=r_hi, zeros=True, endpoints=1)
r = radius.xab[::-1]
vol_avg = NT.volume_avg(KE, r, method=`cheb-end',
                        data_reversed=1)
dVdt = NT.derivative(vr, time)
\end{lstlisting}

\subparagraph{General Remarks}
Each method has its advantages and disadvantages. Some are explicit but require typing
more each time the function is called. Others offer minimal typing per function call, but
have the potential to overwrite other functions of the same name that were imported by
other modules (for example, both {\tt chebyshev} and {\tt differentiate} have a function
called {\tt derivative}). For this reason, I like to use the public interface. Some rare
functions may not be available through the public interface, this just means another method
must be used to import that rare function.

\end{document}

