#!/bin/sh -p
#
# Simple script to install NumericalTools
#

if [ $# -lt 1 ]; then
   echo
   echo "Usage:"
   echo
   echo "    install.sh <NumericalTools-Dir>"
   echo
   echo "    <NumericalTools-Dir> is the home directory for NumericalTools"
   echo
else
   cwd=`pwd`
   NumToolsDir=$1
   cd ${NumToolsDir}
   echo -e "\nInstalling NumericalTools...\n"
   python setup.py install --user --record installed.txt
   echo -e "\nNumericalTools was installed\n"
   cd ${cwd}
fi

