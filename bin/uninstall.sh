#!/bin/sh -p
#
# Simple script to un-install NumericalTools
#

if [ $# -lt 1 ]; then
   echo
   echo "Usage:"
   echo
   echo "    uninstall.sh <NumericalTools-Dir>"
   echo
   echo "    <NumericalTools-Dir> is the home directory for NumericalTools"
   echo
else
   cwd=`pwd`
   NumToolsDir=$1
   cd ${NumToolsDir}
   echo -e "\nUn-installing NumericalTools...\n"
   cat installed.txt | xargs rm -rf
   rm -rf build
   rm -rf dist
   rm -rf NumericalTools.egg-info
   rm -f  installed.txt
   echo -e "\nNumericalTools was uninstalled\n"
   cd ${cwd}
fi

