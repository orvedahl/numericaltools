#!/bin/sh -p
#
# Simple script to re-install NumericalTools
#

if [ $# -lt 1 ]; then
   echo
   echo "Usage:"
   echo
   echo "    reinstall.sh <NumericalTools-Dir>"
   echo
   echo "    <NumericalTools-Dir> is the home directory for NumericalTools"
   echo
else
   cwd=`pwd`
   NumToolsDir=$1
   cd ${NumToolsDir}
   echo -e "\nRe-Installing NumericalTools...\n"
   python setup.py install --user --record installed.txt
   echo -e "\nNumericalTools was reinstalled\n"
   cd ${cwd}
fi

